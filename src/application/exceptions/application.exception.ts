import { CustomError } from "ts-custom-error";

export class ApplicationException extends CustomError {}
