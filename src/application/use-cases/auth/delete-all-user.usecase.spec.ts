/* eslint-disable import/first */
jest.mock("../../../infrastructure/repositories/user.repository.pg", () => {
  return {
    UserRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        deleteAll: jest.fn(),
      };
    }),
  };
});

import "reflect-metadata";
import Container from "typedi";

import { UserRepositoryPg } from "../../../infrastructure/repositories/user.repository.pg";
import { DeleteAllUsersUseCase } from "./delete-all-user.usecase";

describe("Delete all users use case", () => {
  test("should delete all users", async () => {
    const repository = new UserRepositoryPg();
    Container.set("user-repository", repository);

    const useCase: DeleteAllUsersUseCase = Container.get(DeleteAllUsersUseCase);

    await useCase.execute();
    expect(repository.deleteAll).toHaveBeenCalled();
  });
});
