/* eslint-disable import/first */
import { User } from "../../../domain/models/user.entity";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { EmailVO } from "../../../domain/vos/user/email.vo";
import { PasswordVO } from "../../../domain/vos/user/password.vo";
import { Role, RoleVO } from "../../../domain/vos/user/role.vo";

jest.mock("../../../infrastructure/repositories/user.repository.pg", () => {
  return {
    UserRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        save: jest.fn().mockImplementation(
          () =>
            new User({
              id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
              email: EmailVO.create("victor.romero@kairosds.com"),
              password: PasswordVO.create("1234"),
              role: RoleVO.create(Role.USER),
            })
        ),
      };
    }),
  };
});

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";
import { UserRepositoryPg } from "../../../infrastructure/repositories/user.repository.pg";
import { SignUpInput } from "../../types/auth.type";
import { SignUpUseCase } from "./sign-up.usecase";

describe("Sign up user use case", () => {
  test("should create a new user and save it", async () => {
    const repository = new UserRepositoryPg();
    Container.set("user-repository", repository);
    const useCase: SignUpUseCase = Container.get(SignUpUseCase);
    const request: SignUpInput = {
      email: "victor.romero@kairosds.com",
      password: "1234",
      role: "AUTHOR",
    };
    const response = await useCase.execute(request);
    expect(repository.save).toHaveBeenCalled();
    expect(response.email).toBe(request.email);
    expect(validate(response.id)).toBeTruthy();
  });
});
