/* eslint-disable import/first */
import { User } from "../../../domain/models/user.entity";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { EmailVO } from "../../../domain/vos/user/email.vo";
import { PasswordVO } from "../../../domain/vos/user/password.vo";
import { Role, RoleVO } from "../../../domain/vos/user/role.vo";

jest.mock("../../../infrastructure/repositories/user.repository.pg", () => {
  return {
    UserRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        findByEmail: jest.fn().mockImplementation(
          () =>
            new User({
              id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
              email: EmailVO.create("victor.romero@kairosds.com"),
              password: PasswordVO.createFromHash(
                "$2b$10$yWrgIaBqboe12bIzXB6Cd.ad8z6Xe89YPQY8ZLKZqX705FWnNC.2W"
              ),
              role: RoleVO.create(Role.USER),
            })
        ),
      };
    }),
  };
});

import "reflect-metadata";
import Container from "typedi";
import { UserRepositoryPg } from "../../../infrastructure/repositories/user.repository.pg";
import { SignInInput } from "../../types/auth.type";
import { SignInUseCase } from "./sign-in.usecase";

describe("Login user use case", () => {
  test("should login user and deliver a token", async () => {
    const repository = new UserRepositoryPg();
    Container.set("user-repository", repository);
    const useCase: SignInUseCase = Container.get(SignInUseCase);
    const request: SignInInput = {
      email: "victor.romero@kairosds.com",
      password: "1234",
    };
    const response = await useCase.execute(request);
    expect(repository.findByEmail).toHaveBeenCalled();
    expect(response.user_token).toBeTruthy();
    expect(response.refresh_token).toBeTruthy();
  });
});
