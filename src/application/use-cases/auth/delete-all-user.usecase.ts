import { Service } from "typedi";
import { UserService } from "../../../domain/services/user.service";

@Service()
export class DeleteAllUsersUseCase {
  constructor(private service: UserService) {}

  async execute(): Promise<void> {
    await this.service.deleteAll();
  }
}
