import jwt from "jsonwebtoken";

import { Service } from "typedi";
import { UserService } from "../../../domain/services/user.service";
import { EmailVO } from "../../../domain/vos/user/email.vo";
import { PasswordVO } from "../../../domain/vos/user/password.vo";
import { SignInInput, SignInOutput } from "../../types/auth.type";

@Service()
export class SignInUseCase {
  constructor(private service: UserService) {}

  async execute(request: SignInInput): Promise<SignInOutput> {
    const email = EmailVO.create(request.email);
    const user = await this.service.findByEmail(email);
    const plainPassword = PasswordVO.create(request.password);
    await this.service.isValidPassword(plainPassword, user);
    const refreshToken = jwt.sign({ email: user.email.value }, "SECRET", {
      expiresIn: 86400, // 24h
    });
    const userToken = jwt.sign({ email: user.email.value }, "SECRET", {
      // expiresIn: 300, // 5min
      expiresIn: 86400, // 24h
    });
    return { user_token: userToken, refresh_token: refreshToken };
  }
}
