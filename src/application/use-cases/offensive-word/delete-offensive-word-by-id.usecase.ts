import { Service } from "typedi";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { IdInput } from "../../types/shared.type";

@Service()
export class DeleteOffensiveWordByIdUseCase {
  constructor(private service: OffensiveWordService) {}

  async execute(id: IdInput): Promise<void> {
    const idVO = IdVO.createWithUUID(id);
    await this.service.deleteById(idVO);
  }
}
