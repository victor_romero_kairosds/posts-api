/* eslint-disable import/first */
jest.mock(
  "../../../infrastructure/repositories/offensive-word.repository.mongo",
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          deleteAll: jest.fn(),
        };
      }),
    };
  }
);

import "reflect-metadata";
import Container from "typedi";

import { OffensiveWordRepositoryMongo } from "../../../infrastructure/repositories/offensive-word.repository.mongo";
import { DeleteAllOffensiveWordUseCase } from "./delete-all-offensive-word.usecase";

describe("Delete all offensive words use case", () => {
  test("should delete all offensive words", async () => {
    const repository = new OffensiveWordRepositoryMongo();
    Container.set("offensive-word-repository", repository);

    const useCase: DeleteAllOffensiveWordUseCase = Container.get(
      DeleteAllOffensiveWordUseCase
    );

    await useCase.execute();
    expect(repository.deleteAll).toHaveBeenCalled();
  });
});
