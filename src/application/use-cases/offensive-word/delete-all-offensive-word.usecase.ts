import { Service } from "typedi";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";

@Service()
export class DeleteAllOffensiveWordUseCase {
  constructor(private service: OffensiveWordService) {}

  async execute(): Promise<void> {
    await this.service.deleteAll();
  }
}
