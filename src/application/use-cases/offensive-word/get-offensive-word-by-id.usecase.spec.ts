/* eslint-disable import/first */
import { OffensiveWord } from "../../../domain/models/offensive-word.entity";
import { LevelVO } from "../../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../../domain/vos/offensive-word/word.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";

jest.mock(
  "../../../infrastructure/repositories/offensive-word.repository.mongo",
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          findById: jest.fn().mockImplementation(
            () =>
              new OffensiveWord({
                id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
                word: WordVO.create("padreada"),
                level: LevelVO.create(4),
              })
          ),
        };
      }),
    };
  }
);

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";
import { OffensiveWordRepositoryMongo } from "../../../infrastructure/repositories/offensive-word.repository.mongo";
import { GetOffensiveWordByIdUseCase } from "./get-offensive-word-by-id.usecase";

describe("Get all offensive words use case", () => {
  test("should get all offensive words", async () => {
    const repository = new OffensiveWordRepositoryMongo();
    Container.set("offensive-word-repository", repository);

    const useCase: GetOffensiveWordByIdUseCase = Container.get(
      GetOffensiveWordByIdUseCase
    );

    const offensiveWord = await useCase.execute(
      "9932d3cf-3d78-43b4-bdfc-67f4e5790694"
    );

    expect(repository.findById).toHaveBeenCalled();
    expect(validate(offensiveWord.id)).toBeTruthy();
    expect(offensiveWord.word).toBe("padreada");
    expect(offensiveWord.level).toBe(4);
  });
});
