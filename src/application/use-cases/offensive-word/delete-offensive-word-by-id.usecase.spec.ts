/* eslint-disable import/first */
jest.mock(
  "../../../infrastructure/repositories/offensive-word.repository.mongo",
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          deleteById: jest.fn(),
        };
      }),
    };
  }
);

import "reflect-metadata";
import Container from "typedi";

import { OffensiveWordRepositoryMongo } from "../../../infrastructure/repositories/offensive-word.repository.mongo";
import { DeleteOffensiveWordByIdUseCase } from "./delete-offensive-word-by-id.usecase";

describe("Delete offensive words by id use case", () => {
  test("should delete offensive word", async () => {
    const repository = new OffensiveWordRepositoryMongo();
    Container.set("offensive-word-repository", repository);

    const useCase: DeleteOffensiveWordByIdUseCase = Container.get(
      DeleteOffensiveWordByIdUseCase
    );

    await useCase.execute("9932d3cf-3d78-43b4-bdfc-67f4e5790694");
    expect(repository.deleteById).toHaveBeenCalled();
  });
});
