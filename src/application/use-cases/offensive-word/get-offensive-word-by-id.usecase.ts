import { Service } from "typedi";
import { OffensiveWord } from "../../../domain/models/offensive-word.entity";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { OffensiveWordOutput } from "../../types/offensive-word.type";
import { IdInput } from "../../types/shared.type";

@Service()
export class GetOffensiveWordByIdUseCase {
  constructor(private service: OffensiveWordService) {}

  async execute(id: IdInput): Promise<OffensiveWordOutput> {
    const idVO = IdVO.createWithUUID(id);
    const word: OffensiveWord = await this.service.findById(idVO);

    return {
      id: word.id.value,
      word: word.word.value,
      level: word.level.value,
    };
  }
}
