/* eslint-disable import/first */
import { OffensiveWord } from "../../../domain/models/offensive-word.entity";
import { LevelVO } from "../../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../../domain/vos/offensive-word/word.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";

jest.mock(
  "../../../infrastructure/repositories/offensive-word.repository.mongo",
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          findAll: jest.fn().mockImplementation(() => {
            return [
              new OffensiveWord({
                id: IdVO.create(),
                word: WordVO.create("padreada"),
                level: LevelVO.create(4),
              }),
            ];
          }),
        };
      }),
    };
  }
);

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";
import { OffensiveWordRepositoryMongo } from "../../../infrastructure/repositories/offensive-word.repository.mongo";
import { GetAllOffensiveWordUseCase } from "./get-all-offensive-word.usecase";
describe("Get all offensive words use case", () => {
  test("should get all offensive words", async () => {
    const repository = new OffensiveWordRepositoryMongo();
    Container.set("offensive-word-repository", repository);

    const useCase: GetAllOffensiveWordUseCase = Container.get(
      GetAllOffensiveWordUseCase
    );

    const listOfOffensiveWords = await useCase.execute();

    expect(repository.findAll).toHaveBeenCalled();
    expect(listOfOffensiveWords).toHaveLength(1);
    expect(validate(listOfOffensiveWords[0].id)).toBeTruthy();
    expect(listOfOffensiveWords[0].word).toBe("padreada");
    expect(listOfOffensiveWords[0].level).toBe(4);
  });
});
