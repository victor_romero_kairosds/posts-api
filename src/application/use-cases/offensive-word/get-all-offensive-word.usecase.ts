import { Service } from "typedi";
import { OffensiveWord } from "../../../domain/models/offensive-word.entity";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";
import { OffensiveWordOutput } from "../../types/offensive-word.type";

@Service()
export class GetAllOffensiveWordUseCase {
  constructor(private service: OffensiveWordService) {}

  async execute(): Promise<OffensiveWordOutput[]> {
    const listOfWords: OffensiveWord[] = await this.service.findAll();

    return listOfWords.map((word) => {
      return {
        id: word.id.value,
        word: word.word.value,
        level: word.level.value,
      };
    });
  }
}
