import { Service } from "typedi";
import { OffensiveWordType } from "../../../domain/models/offensive-word.entity";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";
import { LevelVO } from "../../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../../domain/vos/offensive-word/word.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import {
  OffensiveWordInput,
  OffensiveWordOutput,
} from "../../types/offensive-word.type";

@Service()
export class CreateOffensiveWordUseCase {
  constructor(private service: OffensiveWordService) {}

  async execute(request: OffensiveWordInput): Promise<OffensiveWordOutput> {
    const newOffensiveWord: OffensiveWordType = {
      id: IdVO.create(),
      word: WordVO.create(request.word),
      level: LevelVO.create(request.level),
    };

    await this.service.persist(newOffensiveWord);

    return {
      id: newOffensiveWord.id.value,
      word: newOffensiveWord.word.value,
      level: newOffensiveWord.level.value,
    };
  }
}
