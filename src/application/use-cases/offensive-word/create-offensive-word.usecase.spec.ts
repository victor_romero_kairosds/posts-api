/* eslint-disable import/first */
import { OffensiveWord } from "../../../domain/models/offensive-word.entity";
import { LevelVO } from "../../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../../domain/vos/offensive-word/word.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";

jest.mock(
  "../../../infrastructure/repositories/offensive-word.repository.mongo",
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          save: jest.fn().mockImplementation(
            () =>
              new OffensiveWord({
                id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
                word: WordVO.create("padreada"),
                level: LevelVO.create(3),
              })
          ),
        };
      }),
    };
  }
);

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";

import { OffensiveWordRepositoryMongo } from "../../../infrastructure/repositories/offensive-word.repository.mongo";
import { OffensiveWordInput } from "../../types/offensive-word.type";
import { CreateOffensiveWordUseCase } from "./create-offensive-word.usecase";
describe("Create a new OffensiveWord use case", () => {
  test("should create a new offensive word and save it", async () => {
    const repository = new OffensiveWordRepositoryMongo();
    Container.set("offensive-word-repository", repository);
    const useCase: CreateOffensiveWordUseCase = Container.get(
      CreateOffensiveWordUseCase
    );
    const request: OffensiveWordInput = {
      word: "padreada",
      level: 3,
    };
    const response = await useCase.execute(request);
    expect(repository.save).toHaveBeenCalled();
    expect(response.word).toBe(request.word);
    expect(response.level).toBe(3);
    expect(validate(response.id)).toBeTruthy();
  });
});
