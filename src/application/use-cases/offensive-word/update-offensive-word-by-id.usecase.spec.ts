/* eslint-disable import/first */
import { OffensiveWord } from "../../../domain/models/offensive-word.entity";
import { LevelVO } from "../../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../../domain/vos/offensive-word/word.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";

jest.mock(
  "../../../infrastructure/repositories/offensive-word.repository.mongo",
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          updateById: jest.fn().mockImplementation(
            () =>
              new OffensiveWord({
                id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
                word: WordVO.create("padreada"),
                level: LevelVO.create(3),
              })
          ),
        };
      }),
    };
  }
);

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";
import { OffensiveWordRepositoryMongo } from "../../../infrastructure/repositories/offensive-word.repository.mongo";
import { IdInput } from "../../types/shared.type";
import { UpdateOffensiveWordByIdUseCase } from "./update-offensive-word-by-id.usecase";

describe("Update offensive word by id usecase", () => {
  test("should update offensive word with the new data", async () => {
    const repository = new OffensiveWordRepositoryMongo();
    Container.set("offensive-word-repository", repository);
    const useCase: UpdateOffensiveWordByIdUseCase = Container.get(
      UpdateOffensiveWordByIdUseCase
    );

    const id: IdInput = "9932d3cf-3d78-43b4-bdfc-67f4e5790694";
    const updatedWord = await useCase.execute(id, {
      word: "padreada",
      level: 3,
    });

    expect(repository.updateById).toHaveBeenCalled();
    expect(updatedWord.id).toBe(id);
    expect(validate(updatedWord.id)).toBeTruthy();
    expect(updatedWord.word).toBe("padreada");
    expect(updatedWord.level).toBe(3);
  });
});
