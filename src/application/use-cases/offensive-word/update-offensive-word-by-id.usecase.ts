import { Service } from "typedi";
import { OffensiveWordType } from "../../../domain/models/offensive-word.entity";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";
import { LevelVO } from "../../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../../domain/vos/offensive-word/word.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import {
  OffensiveWordInput,
  OffensiveWordOutput,
} from "../../types/offensive-word.type";
import { IdInput } from "../../types/shared.type";

@Service()
export class UpdateOffensiveWordByIdUseCase {
  constructor(private service: OffensiveWordService) {}

  async execute(
    id: IdInput,
    updateRequest: OffensiveWordInput
  ): Promise<OffensiveWordOutput> {
    const offensiveWord: OffensiveWordType = {
      id: IdVO.createWithUUID(id),
      word: WordVO.create(updateRequest.word),
      level: LevelVO.create(updateRequest.level),
    };

    await this.service.updateById(offensiveWord);

    return {
      id: offensiveWord.id.value,
      word: offensiveWord.word.value,
      level: offensiveWord.level.value,
    };
  }
}
