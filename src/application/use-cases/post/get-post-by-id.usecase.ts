import { Service } from "typedi";
import { PostService } from "../../../domain/services/post.service";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { PostDetailOutput } from "../../types/post.type";
import { IdInput } from "../../types/shared.type";

@Service()
export class GetPostByIdUseCase {
  constructor(private postService: PostService) {}

  async execute(id: IdInput): Promise<PostDetailOutput> {
    const post = await this.postService.findById(IdVO.createWithUUID(id));

    return {
      id: post.id.value,
      title: post.title.value,
      content: post.content.value,
      author: post.author.email.value,
      comments: post.comments.map((comment) => {
        return {
          id: comment.id.value,
          content: comment.content.value,
          author: comment.author.email.value,
        };
      }),
    };
  }
}
