import { Service } from "typedi";
import { PostService } from "../../../domain/services/post.service";

@Service()
export class DeleteAllPostUseCase {
  constructor(private postService: PostService) {}

  async execute(): Promise<void> {
    await this.postService.deleteAll();
  }
}
