import { Service } from "typedi";
import { PostType } from "../../../domain/models/post.entity";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";
import { PostService } from "../../../domain/services/post.service";
import { ContentVO } from "../../../domain/vos/post/content.vo";
import { TitleVO } from "../../../domain/vos/post/title.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { PostDetailOutput, PostInput } from "../../types/post.type";

@Service()
export class CreatePostUseCase {
  constructor(
    private postService: PostService,
    private offensiveWordService: OffensiveWordService
  ) {}

  async execute(request: PostInput): Promise<PostDetailOutput> {
    const offensiveWordsList = await this.offensiveWordService.findAll();

    const newPost: PostType = {
      id: IdVO.create(),
      title: TitleVO.create(request.title),
      content: ContentVO.create(request.content, offensiveWordsList),
      author: request.author,
      comments: [],
    };

    await this.postService.persist(newPost);

    return {
      id: newPost.id.value,
      title: newPost.title.value,
      content: newPost.content.value,
      author: newPost.author.email.value,
      comments: [],
    };
  }
}
