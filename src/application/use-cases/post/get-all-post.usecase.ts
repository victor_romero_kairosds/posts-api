import { Service } from "typedi";
import { Post } from "../../../domain/models/post.entity";
import { PostService } from "../../../domain/services/post.service";
import { PostOutput } from "../../types/post.type";

@Service()
export class GetAllPostUseCase {
  constructor(private postService: PostService) {}

  async execute(): Promise<PostOutput[]> {
    const posts = await this.postService.findAll();

    return posts.map((post: Post) => {
      return {
        id: post.id.value,
        title: post.title.value,
        content: post.content.value,
        author: post.author.email.value,
      };
    });
  }
}
