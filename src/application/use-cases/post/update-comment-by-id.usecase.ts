import { Service } from "typedi";
import { CommentType } from "../../../domain/models/comment.entity";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";
import { PostService } from "../../../domain/services/post.service";
import { ContentVO } from "../../../domain/vos/post/content.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { CommentInput, CommentOutput } from "../../types/post.type";
import { IdInput } from "../../types/shared.type";

@Service()
export class UpdateCommentByIdUseCase {
  constructor(
    private postService: PostService,
    private offensiveWordService: OffensiveWordService
  ) {}

  async execute(
    request: CommentInput,
    commentId: IdInput,
    postId: IdInput
  ): Promise<CommentOutput> {
    const offensiveWords = await this.offensiveWordService.findAll();

    const comment: CommentType = {
      id: IdVO.createWithUUID(commentId),
      content: ContentVO.create(request.content, offensiveWords),
      author: request.author,
    };

    await this.postService.updateComment(comment, IdVO.createWithUUID(postId));

    return {
      id: comment.id.value,
      content: comment.content.value,
      author: comment.author.email.value,
    };
  }
}
