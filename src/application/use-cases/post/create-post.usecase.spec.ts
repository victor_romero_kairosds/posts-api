/* eslint-disable import/first */
import { OffensiveWord } from "../../../domain/models/offensive-word.entity";
import { User } from "../../../domain/models/user.entity";
import { LevelVO } from "../../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../../domain/vos/offensive-word/word.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { EmailVO } from "../../../domain/vos/user/email.vo";
import { PasswordVO } from "../../../domain/vos/user/password.vo";
import { Role, RoleVO } from "../../../domain/vos/user/role.vo";

jest.mock("../../../infrastructure/repositories/post.repository.pg", () => {
  return {
    PostRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        save: jest.fn().mockImplementation(),
      };
    }),
  };
});

jest.mock(
  "../../../infrastructure/repositories/offensive-word.repository.mongo",
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          findAll: jest.fn().mockImplementation(() => {
            return [
              new OffensiveWord({
                id: IdVO.create(),
                word: WordVO.create("padreada"),
                level: LevelVO.create(4),
              }),
            ];
          }),
        };
      }),
    };
  }
);

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";

import { OffensiveWordRepositoryMongo } from "../../../infrastructure/repositories/offensive-word.repository.mongo";
import { PostRepositoryPg } from "../../../infrastructure/repositories/post.repository.pg";
import { PostInput } from "../../types/post.type";
import { CreatePostUseCase } from "./create-post.usecase";
describe("Create a new Post use case", () => {
  test("should create a new post and save it", async () => {
    const postRepository = new PostRepositoryPg();
    const offensiveWordRepository = new OffensiveWordRepositoryMongo();

    Container.set("post-repository", postRepository);
    Container.set("offensive-word-repository", offensiveWordRepository);

    const useCase: CreatePostUseCase = Container.get(CreatePostUseCase);
    const request: PostInput = {
      title: "this is a different title",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula.",
      author: new User({
        id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
        email: EmailVO.create("victor.romero@kairosds.com"),
        password: PasswordVO.create("1234"),
        role: RoleVO.create(Role.ADMIN),
      }),
    };
    const post = await useCase.execute(request);
    expect(postRepository.save).toHaveBeenCalled();
    expect(offensiveWordRepository.findAll).toHaveBeenCalled();

    expect(post.title).toBe(request.title);
    expect(post.content).toBe(request.content);
    expect(post.author).toBe(request.author.email.value);
    expect(post.comments).toEqual([]);
    expect(validate(post.id)).toBeTruthy();
  });
});
