import { Service } from "typedi";
import { PostService } from "../../../domain/services/post.service";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { IdInput } from "../../types/shared.type";

@Service()
export class DeletePostByIdUseCase {
  constructor(private postService: PostService) {}

  async execute(id: IdInput): Promise<void> {
    await this.postService.deleteById(IdVO.createWithUUID(id));
  }
}
