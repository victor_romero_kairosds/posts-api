/* eslint-disable import/first */
jest.mock("../../../infrastructure/repositories/post.repository.pg", () => {
  return {
    PostRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        deleteAll: jest.fn(),
      };
    }),
  };
});

import "reflect-metadata";
import Container from "typedi";

import { PostRepositoryPg } from "../../../infrastructure/repositories/post.repository.pg";
import { DeleteAllPostUseCase } from "./delete-all-post.usecase";

describe("Delete all posts use case", () => {
  test("should delete all posts", async () => {
    const repository = new PostRepositoryPg();
    Container.set("post-repository", repository);

    const useCase: DeleteAllPostUseCase = Container.get(DeleteAllPostUseCase);

    await useCase.execute();
    expect(repository.deleteAll).toHaveBeenCalled();
  });
});
