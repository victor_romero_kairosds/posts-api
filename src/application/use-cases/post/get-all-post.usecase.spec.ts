import { Comment } from "../../../domain/models/comment.entity";
import { Post } from "../../../domain/models/post.entity";
import { User } from "../../../domain/models/user.entity";
import { ContentVO } from "../../../domain/vos/post/content.vo";
import { TitleVO } from "../../../domain/vos/post/title.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { EmailVO } from "../../../domain/vos/user/email.vo";
import { PasswordVO } from "../../../domain/vos/user/password.vo";
import { Role, RoleVO } from "../../../domain/vos/user/role.vo";

/* eslint-disable import/first */
jest.mock("../../../infrastructure/repositories/post.repository.pg", () => {
  return {
    PostRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        findAll: jest.fn().mockImplementation(() => {
          return [
            new Post({
              id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
              title: TitleVO.create("Lorem ipsum dolor"),
              content: ContentVO.create(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
              ),
              author: new User({
                id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
                email: EmailVO.create("victor.romero@kairosds.com"),
                password: PasswordVO.create("1234"),
                role: RoleVO.create(Role.ADMIN),
              }),
              comments: [
                new Comment({
                  id: IdVO.createWithUUID(
                    "9932d3cf-3d78-43b4-bdfc-67f4e5790694"
                  ),
                  content: ContentVO.create("Lorem ipsum dolor sit amet"),
                  author: new User({
                    id: IdVO.createWithUUID(
                      "9932d3cf-3d78-43b4-bdfc-67f4e5790694"
                    ),
                    email: EmailVO.create("victor.romero@kairosds.com"),
                    password: PasswordVO.create("1234"),
                    role: RoleVO.create(Role.ADMIN),
                  }),
                }),
              ],
            }),
          ];
        }),
      };
    }),
  };
});

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";
import { PostRepositoryPg } from "../../../infrastructure/repositories/post.repository.pg";
import { GetAllPostUseCase } from "./get-all-post.usecase";

describe("Get all posts use case", () => {
  test("should get all posts", async () => {
    const repository = new PostRepositoryPg();
    Container.set("post-repository", repository);
    const useCase: GetAllPostUseCase = Container.get(GetAllPostUseCase);

    const listOfPosts = await useCase.execute();

    expect(repository.findAll).toHaveBeenCalled();
    expect(listOfPosts).toHaveLength(1);
    expect(validate(listOfPosts[0].id)).toBeTruthy();
    expect(listOfPosts[0].title).toBe("Lorem ipsum dolor");
    expect(listOfPosts[0].content).toBe(
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
    );
    expect(listOfPosts[0].author).toBe("victor.romero@kairosds.com");
  });
});
