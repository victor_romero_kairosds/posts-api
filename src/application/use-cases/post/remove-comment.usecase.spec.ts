/* eslint-disable import/first */
import { Comment } from "../../../domain/models/comment.entity";
import { Post } from "../../../domain/models/post.entity";
import { User } from "../../../domain/models/user.entity";
import { ContentVO } from "../../../domain/vos/post/content.vo";
import { TitleVO } from "../../../domain/vos/post/title.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { EmailVO } from "../../../domain/vos/user/email.vo";
import { PasswordVO } from "../../../domain/vos/user/password.vo";
import { Role, RoleVO } from "../../../domain/vos/user/role.vo";

const user = new User({
  id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
  email: EmailVO.create("victor.romero@kairosds.com"),
  password: PasswordVO.create("1234"),
  role: RoleVO.create(Role.ADMIN),
});

jest.mock("../../../infrastructure/repositories/post.repository.pg", () => {
  return {
    PostRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        removeComment: jest.fn(),
        findById: jest.fn().mockImplementation(
          () =>
            new Post({
              id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
              title: TitleVO.create("Lorem ipsum dolor"),
              content: ContentVO.create(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
              ),
              author: user,
              comments: [
                new Comment({
                  id: IdVO.create(),
                  content: ContentVO.create("Lorem ipsum dolor sit amet."),
                  author: user,
                }),
              ],
            })
        ),
        findCommentById: jest.fn().mockImplementation(
          () =>
            new Comment({
              id: IdVO.create(),
              content: ContentVO.create("Lorem ipsum dolor sit amet."),
              author: user,
            })
        ),
      };
    }),
  };
});

import "reflect-metadata";
import Container from "typedi";

import { PostRepositoryPg } from "../../../infrastructure/repositories/post.repository.pg";
import { RemoveCommentUseCase } from "./remove-comment.usecase";

describe("Remove a comment use case", () => {
  test("should remove an specific comment", async () => {
    const repository = new PostRepositoryPg();
    Container.set("post-repository", repository);

    const useCase: RemoveCommentUseCase = Container.get(RemoveCommentUseCase);

    await useCase.execute(
      "9932d3cf-3d78-43b4-bdfc-67f4e5790694",
      "9932d3cf-3d78-43b4-bdfc-67f4e5790694"
    );
    expect(repository.removeComment).toHaveBeenCalled();
  });
});
