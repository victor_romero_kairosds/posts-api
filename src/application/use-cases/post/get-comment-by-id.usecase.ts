import { Service } from "typedi";
import { PostService } from "../../../domain/services/post.service";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { CommentOutput } from "../../types/post.type";
import { IdInput } from "../../types/shared.type";

@Service()
export class GetCommentByIdUseCase {
  constructor(private postService: PostService) {}

  async execute(id: IdInput): Promise<CommentOutput> {
    const comment = await this.postService.findComment(IdVO.createWithUUID(id));

    return {
      id: comment.id.value,
      content: comment.content.value,
      author: comment.author.email.value,
    };
  }
}
