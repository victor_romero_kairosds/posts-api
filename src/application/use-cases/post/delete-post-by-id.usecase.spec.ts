/* eslint-disable import/first */
jest.mock("../../../infrastructure/repositories/post.repository.pg", () => {
  return {
    PostRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        deleteById: jest.fn(),
      };
    }),
  };
});

import "reflect-metadata";
import Container from "typedi";

import { PostRepositoryPg } from "../../../infrastructure/repositories/post.repository.pg";
import { DeletePostByIdUseCase } from "./delete-post-by-id.usecase";

describe("Delete post by id use case", () => {
  test("should delete an specific post", async () => {
    const repository = new PostRepositoryPg();
    Container.set("post-repository", repository);

    const useCase: DeletePostByIdUseCase = Container.get(DeletePostByIdUseCase);

    await useCase.execute("9932d3cf-3d78-43b4-bdfc-67f4e5790694");
    expect(repository.deleteById).toHaveBeenCalled();
  });
});
