import { Service } from "typedi";
import { PostService } from "../../../domain/services/post.service";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { IdInput } from "../../types/shared.type";

@Service()
export class RemoveCommentUseCase {
  constructor(private postService: PostService) {}

  async execute(commentId: IdInput, postId: IdInput): Promise<void> {
    await this.postService.removeComment(
      IdVO.createWithUUID(commentId),
      IdVO.createWithUUID(postId)
    );
  }
}
