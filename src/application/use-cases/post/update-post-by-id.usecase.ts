import { Service } from "typedi";
import { Post, PostType } from "../../../domain/models/post.entity";
import { OffensiveWordService } from "../../../domain/services/offensive-word.service";
import { PostService } from "../../../domain/services/post.service";
import { ContentVO } from "../../../domain/vos/post/content.vo";
import { TitleVO } from "../../../domain/vos/post/title.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { PostDetailOutput, PostInput } from "../../types/post.type";
import { IdInput } from "../../types/shared.type";

@Service()
export class UpdatePostByIdUseCase {
  constructor(
    private postService: PostService,
    private offensiveWordService: OffensiveWordService
  ) {}

  async execute(
    id: IdInput,
    updateInput: PostInput
  ): Promise<PostDetailOutput> {
    const post = await this.postService.findById(IdVO.createWithUUID(id));
    const offensiveWordsList = await this.offensiveWordService.findAll();

    const postData: PostType = {
      id: IdVO.createWithUUID(id),
      title: TitleVO.create(updateInput.title),
      content: ContentVO.create(updateInput.content, offensiveWordsList),
      author: post.author,
      comments: post.comments,
    };

    await this.postService.updateById(new Post(postData));

    return {
      id: postData.id.value,
      title: postData.title.value,
      content: postData.content.value,
      author: postData.author.email.value,
      comments: postData.comments.map((comment) => {
        return {
          id: comment.id.value,
          content: comment.content.value,
          author: comment.author.email.value,
        };
      }),
    };
  }
}
