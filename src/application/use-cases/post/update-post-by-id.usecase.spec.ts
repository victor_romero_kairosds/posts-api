/* eslint-disable import/first */
import { Comment } from "../../../domain/models/comment.entity";
import { OffensiveWord } from "../../../domain/models/offensive-word.entity";
import { Post } from "../../../domain/models/post.entity";
import { User } from "../../../domain/models/user.entity";
import { LevelVO } from "../../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../../domain/vos/offensive-word/word.vo";
import { ContentVO } from "../../../domain/vos/post/content.vo";
import { TitleVO } from "../../../domain/vos/post/title.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { EmailVO } from "../../../domain/vos/user/email.vo";
import { PasswordVO } from "../../../domain/vos/user/password.vo";
import { Role, RoleVO } from "../../../domain/vos/user/role.vo";

const user = new User({
  id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
  email: EmailVO.create("victor.romero@kairosds.com"),
  password: PasswordVO.create("1234"),
  role: RoleVO.create(Role.ADMIN),
});

jest.mock("../../../infrastructure/repositories/post.repository.pg", () => {
  return {
    PostRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        updateById: jest.fn().mockImplementation(),
        findById: jest.fn().mockImplementation(
          () =>
            new Post({
              id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
              title: TitleVO.create("Lorem ipsum dolor"),
              content: ContentVO.create(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
              ),
              author: user,
              comments: [
                new Comment({
                  id: IdVO.create(),
                  content: ContentVO.create("Lorem ipsum dolor sit amet."),
                  author: user,
                }),
              ],
            })
        ),
      };
    }),
  };
});

jest.mock(
  "../../../infrastructure/repositories/offensive-word.repository.mongo",
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          findAll: jest.fn().mockImplementation(() => {
            return [
              new OffensiveWord({
                id: IdVO.create(),
                word: WordVO.create("padreada"),
                level: LevelVO.create(4),
              }),
            ];
          }),
        };
      }),
    };
  }
);

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";
import { OffensiveWordRepositoryMongo } from "../../../infrastructure/repositories/offensive-word.repository.mongo";
import { PostRepositoryPg } from "../../../infrastructure/repositories/post.repository.pg";
import { UpdatePostByIdUseCase } from "./update-post-by-id.usecase";

describe("Update post by id use case", () => {
  test("should update an specific post", async () => {
    const postRepository = new PostRepositoryPg();
    const offensiveWordRepository = new OffensiveWordRepositoryMongo();

    Container.set("post-repository", postRepository);
    Container.set("offensive-word-repository", offensiveWordRepository);

    const useCase: UpdatePostByIdUseCase = Container.get(UpdatePostByIdUseCase);

    const postInput = {
      title: "Lorem ipsum dolor",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula.",
      author: new User({
        id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
        email: EmailVO.create("victor.romero@kairosds.com"),
        password: PasswordVO.create("1234"),
        role: RoleVO.create(Role.ADMIN),
      }),
    };
    const post = await useCase.execute(
      "9932d3cf-3d78-43b4-bdfc-67f4e5790694",
      postInput
    );
    expect(postRepository.findById).toHaveBeenCalled();
    expect(offensiveWordRepository.findAll).toHaveBeenCalled();
    expect(postRepository.updateById).toHaveBeenCalled();

    expect(validate(post.id)).toBeTruthy();
    expect(post.title).toBe("Lorem ipsum dolor");
    expect(post.content).toBe(
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
    );
    expect(post.comments).toHaveLength(1);
    expect(post.comments[0].content).toBe("Lorem ipsum dolor sit amet.");
    expect(post.comments[0].author).toBe("victor.romero@kairosds.com");
    expect(post.author).toBe("victor.romero@kairosds.com");
  });
});
