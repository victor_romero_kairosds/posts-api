/* eslint-disable import/first */
import { Comment } from "../../../domain/models/comment.entity";
import { User } from "../../../domain/models/user.entity";
import { ContentVO } from "../../../domain/vos/post/content.vo";
import { IdVO } from "../../../domain/vos/shared/id.vo";
import { EmailVO } from "../../../domain/vos/user/email.vo";
import { PasswordVO } from "../../../domain/vos/user/password.vo";
import { Role, RoleVO } from "../../../domain/vos/user/role.vo";

const user = new User({
  id: IdVO.create(),
  email: EmailVO.create("victor.romero@kairosds.com"),
  password: PasswordVO.create("1234"),
  role: RoleVO.create(Role.ADMIN),
});

jest.mock("../../../infrastructure/repositories/post.repository.pg", () => {
  return {
    PostRepositoryPg: jest.fn().mockImplementation(() => {
      return {
        findCommentById: jest.fn().mockImplementation(
          () =>
            new Comment({
              id: IdVO.createWithUUID("9932d3cf-3d78-43b4-bdfc-67f4e5790694"),
              content: ContentVO.create("Lorem ipsum dolor sit amet"),
              author: user,
            })
        ),
      };
    }),
  };
});

import "reflect-metadata";
import Container from "typedi";
import { validate } from "uuid";
import { PostRepositoryPg } from "../../../infrastructure/repositories/post.repository.pg";
import { GetCommentByIdUseCase } from "./get-comment-by-id.usecase";

describe("Get comment by id use case", () => {
  test("should get a comment by id", async () => {
    const repository = new PostRepositoryPg();
    Container.set("post-repository", repository);
    const useCase: GetCommentByIdUseCase = Container.get(GetCommentByIdUseCase);

    const comment = await useCase.execute(
      "9932d3cf-3d78-43b4-bdfc-67f4e5790694"
    );

    expect(repository.findCommentById).toHaveBeenCalled();
    expect(comment.content).toBe("Lorem ipsum dolor sit amet");
    expect(comment.author).toBe("victor.romero@kairosds.com");
    expect(comment.id).toBe("9932d3cf-3d78-43b4-bdfc-67f4e5790694");
    expect(validate(comment.id)).toBeTruthy();
  });
});
