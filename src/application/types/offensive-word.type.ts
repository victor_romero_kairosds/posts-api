export type OffensiveWordInput = {
  word: string;
  level: number;
};

export type OffensiveWordOutput = {
  id: string;
  word: string;
  level: number;
};
