export type SignInInput = {
  email: string;
  password: string;
};

export type SignInOutput = {
  user_token: string;
  refresh_token: string;
};

export type SignUpInput = {
  email: string;
  password: string;
  role: "AUTHOR" | "USER" | "ADMIN";
};

export type SignUpOutput = {
  id: string;
  email: string;
};
