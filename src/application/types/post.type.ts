import { User } from "../../domain/models/user.entity";

export type CommentInput = {
  content: string;
  author: User;
};

export type CommentOutput = {
  id: string;
  content: string;
  author: string;
};

export type PostInput = {
  title: string;
  content: string;
  author: User;
};

export type PostOutput = {
  id: string;
  title: string;
  content: string;
  author: string;
};

export type PostDetailOutput = {
  id: string;
  title: string;
  content: string;
  author: string;
  comments: CommentOutput[];
};
