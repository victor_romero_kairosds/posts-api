import { DomainFormatException } from "./domain-format.exception";

export class PasswordMissmatchException extends DomainFormatException {
  constructor(constructorName: string) {
    super(`${constructorName} -> Password missmatch`);
  }
}
