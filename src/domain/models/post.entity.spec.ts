import { ContentVO } from "../vos/post/content.vo";
import { TitleVO } from "../vos/post/title.vo";
import { IdVO } from "../vos/shared/id.vo";
import { EmailVO } from "../vos/user/email.vo";
import { PasswordVO } from "../vos/user/password.vo";
import { Role, RoleVO } from "../vos/user/role.vo";
import { Comment } from "./comment.entity";

import { Post, PostType } from "./post.entity";
import { User } from "./user.entity";
describe("Post entity", () => {
  let user: User;
  beforeEach(() => {
    user = new User({
      id: IdVO.create(),
      email: EmailVO.create("victor.romero@kairosds.com"),
      password: PasswordVO.create("1234"),
      role: RoleVO.create(Role.USER),
    });
  });
  test("should create a Post entity", () => {
    const data: PostType = {
      id: IdVO.create(),
      title: TitleVO.create("Lorem ipsum dolor"),
      content: ContentVO.create(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
      ),
      author: user,
      comments: [],
    };
    const post = new Post(data);

    expect(post.id.value).toBe(data.id.value);
    expect(post.title.value).toBe(data.title.value);
    expect(post.content.value).toBe(data.content.value);
    expect(post.author).toEqual(user);
    expect(post.comments).toEqual([]);
  });

  test("can add and remove comments in the Post entity", () => {
    const data: PostType = {
      id: IdVO.create(),
      title: TitleVO.create("Lorem ipsum dolor"),
      content: ContentVO.create(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
      ),
      author: user,
      comments: [],
    };
    const post = new Post(data);

    const comment = new Comment({
      id: IdVO.create(),
      content: ContentVO.create("Lorem ipsum dolor"),
      author: user,
    });
    post.addComment(comment);

    expect(post.comments).toHaveLength(1);
    expect(post.comments[0]).toEqual(comment);

    post.removeComment(comment.id);
    expect(post.comments).toHaveLength(0);
    expect(post.comments).toEqual([]);
  });

  test("can get a comment from the Post entity", () => {
    const data: PostType = {
      id: IdVO.create(),
      title: TitleVO.create("Lorem ipsum dolor"),
      content: ContentVO.create(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
      ),
      author: user,
      comments: [],
    };
    const post = new Post(data);

    const comment = new Comment({
      id: IdVO.create(),
      content: ContentVO.create("Lorem ipsum dolor"),
      author: user,
    });
    post.addComment(comment);

    const addedComment = post.getCommentById(comment.id);
    expect(addedComment).toEqual(comment);

    const secondAddedComment = post.getCommentById(IdVO.create());
    expect(secondAddedComment).toBeNull();
  });
});
