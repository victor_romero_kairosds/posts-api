import { WordVO } from "../vos/offensive-word/word.vo";
import { IdVO } from "../vos/shared/id.vo";

import { LevelVO } from "../vos/offensive-word/level.vo";
import { OffensiveWord, OffensiveWordType } from "./offensive-word.entity";
describe("OffensiveWord entity", () => {
  test("should create an OffensiveWord entity", () => {
    const data: OffensiveWordType = {
      id: IdVO.create(),
      word: WordVO.create("padreada"),
      level: LevelVO.create(3),
    };
    const word = new OffensiveWord(data);

    expect(word.id.value).toBe(data.id.value);
    expect(word.word.value).toBe(data.word.value);
    expect(word.level.value).toBe(data.level.value);
  });
});
