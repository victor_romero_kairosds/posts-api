import { IdVO } from "../vos/shared/id.vo";
import { EmailVO } from "../vos/user/email.vo";
import { PasswordVO } from "../vos/user/password.vo";
import { Role, RoleVO } from "../vos/user/role.vo";

import { User, UserType } from "./user.entity";
describe("User entity", () => {
  test("should create a User entity", () => {
    const data: UserType = {
      id: IdVO.create(),
      email: EmailVO.create("victor.romero@kairosds.com"),
      password: PasswordVO.create("1234"),
      role: RoleVO.create(Role.ADMIN),
    };
    const user = new User(data);

    expect(user.id.value).toBe(data.id.value);
    expect(user.email.value).toBe(data.email.value);
    expect(user.password.value).toBe(data.password.value);
    expect(user.role.value).toBe(data.role.value);
  });
});
