import { ContentVO } from "../vos/post/content.vo";
import { TitleVO } from "../vos/post/title.vo";
import { IdVO } from "../vos/shared/id.vo";
import { Comment } from "./comment.entity";
import { User } from "./user.entity";

export type PostType = {
  id: IdVO;
  title: TitleVO;
  content: ContentVO;
  author: User;
  comments: Comment[];
};
export class Post {
  constructor(private post: PostType) {}

  get id(): IdVO {
    return this.post.id;
  }

  get title(): TitleVO {
    return this.post.title;
  }

  get content(): ContentVO {
    return this.post.content;
  }

  get author(): User {
    return this.post.author;
  }

  get comments(): Comment[] {
    return this.post.comments;
  }

  addComment(comment: Comment): void {
    this.post.comments = [...this.post.comments, comment];
  }

  removeComment(commentId: IdVO): void {
    this.post.comments = this.post.comments.filter(
      (c) => c.id.value !== commentId.value
    );
  }

  getCommentById(commentId: IdVO): Comment | null {
    const comment = this.post.comments.find(
      (c) => c.id.value === commentId.value
    );
    if (!comment) return null;
    return comment;
  }
}
