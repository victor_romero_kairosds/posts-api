import { ContentVO } from "../vos/post/content.vo";
import { IdVO } from "../vos/shared/id.vo";
import { EmailVO } from "../vos/user/email.vo";
import { PasswordVO } from "../vos/user/password.vo";
import { Role, RoleVO } from "../vos/user/role.vo";
import { Comment, CommentType } from "./comment.entity";
import { User } from "./user.entity";

describe("Comment entity", () => {
  let user: User;
  beforeEach(() => {
    user = new User({
      id: IdVO.create(),
      email: EmailVO.create("victor.romero@kairosds.com"),
      password: PasswordVO.create("1234"),
      role: RoleVO.create(Role.USER),
    });
  });
  test("should create a Comment entity", () => {
    const data: CommentType = {
      id: IdVO.create(),
      content: ContentVO.create("Lorem ipsum dolor 😎"),
      author: user,
    };
    const comment = new Comment(data);
    expect(comment.id.value).toBe(data.id.value);
    expect(comment.content.value).toBe(data.content.value);
    expect(comment.author).toEqual(user);
  });
});
