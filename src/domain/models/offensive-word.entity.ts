import { LevelVO } from "../vos/offensive-word/level.vo";
import { WordVO } from "../vos/offensive-word/word.vo";
import { IdVO } from "../vos/shared/id.vo";

export type OffensiveWordType = {
  id: IdVO;
  word: WordVO;
  level: LevelVO;
};

export class OffensiveWord {
  constructor(private ow: OffensiveWordType) {}

  get id(): IdVO {
    return this.ow.id;
  }

  get word(): WordVO {
    return this.ow.word;
  }

  get level(): LevelVO {
    return this.ow.level;
  }
}
