import { ContentVO } from "../vos/post/content.vo";
import { IdVO } from "../vos/shared/id.vo";
import { User } from "./user.entity";

export type CommentType = {
  id: IdVO;
  content: ContentVO;
  author: User;
};
export class Comment {
  constructor(private comment: CommentType) {}

  get id(): IdVO {
    return this.comment.id;
  }

  get content(): ContentVO {
    return this.comment.content;
  }

  get author(): User {
    return this.comment.author;
  }
}
