import { VOFormatException } from "../../exceptions/vo-format.exception";
import { WordVO } from "./word.vo";
describe("WordVO", () => {
  test("should create", () => {
    const wordValue = "padreada";
    const word = WordVO.create(wordValue);
    expect(word.value).toBe(wordValue);
  });

  test("should remove accents and special characters", () => {
    const wordValue = "pàdréä·d·a";
    const word = WordVO.create(wordValue);
    expect(word.value).toBe("padreada");
  });

  test("should transform the word to lowercase", () => {
    const wordValue = "PADREADA";
    const word = WordVO.create(wordValue);
    expect(word.value).toBe("padreada");
  });

  test("should remove empty spaces", () => {
    const wordValue = "                    padreada";
    const word = WordVO.create(wordValue);
    expect(word.value).toBe("padreada");
  });

  test("should throw an Error if length is lower than 2", () => {
    const wordValue = "";
    const errorThrower = () => WordVO.create(wordValue);
    expect(errorThrower).toThrowError(VOFormatException);
    expect(errorThrower).toThrowError(
      "WordVO -> Invalid value '' (must have 2 or more characters)"
    );
  });
});
