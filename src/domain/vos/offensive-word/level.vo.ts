import { VOFormatException } from "../../exceptions/vo-format.exception";

export class LevelVO {
  private constructor(private level: number) {}

  static create(level: number) {
    if (level < 1 || level > 5) {
      throw new VOFormatException(
        LevelVO.name,
        level,
        "has to be between 1 and 5"
      );
    }
    return new LevelVO(level);
  }

  get value(): number {
    return this.level;
  }
}
