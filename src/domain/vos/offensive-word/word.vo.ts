import { VOFormatException } from "../../exceptions/vo-format.exception";

export class WordVO {
  get value(): string {
    return this.word;
  }

  private constructor(private word: string) {}

  static create(word: string): WordVO {
    const formattedWord = word
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toLowerCase()
      .trim()
      .replace(/[^a-z0-9 ]/g, "");

    if (formattedWord.length < 2) {
      throw new VOFormatException(
        WordVO.name,
        word,
        "must have 2 or more characters"
      );
    }
    return new WordVO(formattedWord);
  }
}
