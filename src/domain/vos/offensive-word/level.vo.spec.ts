import { VOFormatException } from "../../exceptions/vo-format.exception";
import { LevelVO } from "./level.vo";

describe("Level Value-Object", () => {
  test("should create", () => {
    const levelValue = 1;
    const level = LevelVO.create(levelValue);
    expect(level.value).toBe(levelValue);
  });

  test("should throw an Error if level is greater than 5", () => {
    const invalidLevelValue = 6;

    const errorThrower = () => LevelVO.create(invalidLevelValue);
    expect(errorThrower).toThrowError(VOFormatException);
    expect(errorThrower).toThrowError(
      "LevelVO -> Invalid value '6' (has to be between 1 and 5)"
    );
  });

  test("should throw an Error if level is lower than 0", () => {
    const invalidLevelValue = 0;

    const errorThrower = () => LevelVO.create(invalidLevelValue);
    expect(errorThrower).toThrowError(VOFormatException);
    expect(errorThrower).toThrowError(
      "LevelVO -> Invalid value '0' (has to be between 1 and 5)"
    );
  });
});
