/* eslint-disable no-unused-vars */
export enum Role {
  ADMIN = "ADMIN",
  USER = "USER",
  AUTHOR = "AUTHOR",
}

export class RoleVO {
  private constructor(private role: Role) {}

  static create(role: Role) {
    return new RoleVO(role);
  }

  get value(): Role {
    return this.role;
  }
}
