import { PasswordVO } from "./password.vo";
describe("Password Value-Object", () => {
  test("should create", () => {
    const passwordValue = "1234";
    const password = PasswordVO.create(passwordValue);
    expect(password.value).toBe(passwordValue);
  });
});
