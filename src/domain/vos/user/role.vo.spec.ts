import { Role, RoleVO } from "./role.vo";
describe("Role Value-Object", () => {
  test("should create", () => {
    const roleValue = Role.ADMIN;
    const role = RoleVO.create(roleValue);
    expect(role.value).toBe(roleValue);
  });
});
