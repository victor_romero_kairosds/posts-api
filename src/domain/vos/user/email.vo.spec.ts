import { VOFormatException } from "../../exceptions/vo-format.exception";
import { EmailVO } from "./email.vo";
describe("Email Value-Object", () => {
  test("should create", () => {
    const emailValue = "your.email@gmail.com";
    const email = EmailVO.create(emailValue);
    expect(email.value).toBe(emailValue);
  });

  test("should throw an Error if it isn`t a valid email", () => {
    const invalidEmail = "invalid.email.@gmail.com.";
    const errorThrower = () => EmailVO.create(invalidEmail);
    expect(errorThrower).toThrowError(VOFormatException);
    expect(errorThrower).toThrowError(
      `EmailVO -> Invalid value '${invalidEmail}' (has to be a valid email address)`
    );
  });
});
