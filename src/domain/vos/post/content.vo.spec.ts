import { VOFormatException } from "../../exceptions/vo-format.exception";
import { OffensiveWord } from "../../models/offensive-word.entity";
import { LevelVO } from "../offensive-word/level.vo";
import { WordVO } from "../offensive-word/word.vo";
import { IdVO } from "../shared/id.vo";
import { ContentVO } from "./content.vo";
describe("Content Value-Object", () => {
  test("should create", () => {
    const contentValue = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula. Donec ut rhoncus elit, ut ultricies mi. Vivamus et arcu venenatis libero viverra faucibus.`;

    const content = ContentVO.create(contentValue);
    expect(content.value).toBe(contentValue);
  });
  test("should remove empty spaces", () => {
    const contentValue = `           Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula.`;
    const content = ContentVO.create(contentValue);
    expect(content.value).toBe(
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero nulla, suscipit vel tellus ac, varius volutpat ligula."
    );
  });
  test("should throw an Error if length is lower than 2", () => {
    const contentValue = "L";

    const errorThrower = () => ContentVO.create(contentValue);
    expect(errorThrower).toThrowError(VOFormatException);
    expect(errorThrower).toThrowError(
      "ContentVO -> Invalid value 'L' (must have 2 or more characters)"
    );
  });
  test("should throw an Error if it contains an offensive word", () => {
    const contentValue = "random message with Damn";

    const offensiveWords = [
      new OffensiveWord({
        id: IdVO.create(),
        level: LevelVO.create(3),
        word: WordVO.create("damn"),
      }),
    ];

    const errorThrower = () => ContentVO.create(contentValue, offensiveWords);
    expect(errorThrower).toThrowError(VOFormatException);
    expect(errorThrower).toThrowError(
      "ContentVO -> Invalid value 'damn' (cannot have any offensive word)"
    );
  });
});
