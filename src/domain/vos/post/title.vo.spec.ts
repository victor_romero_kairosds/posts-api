import { VOFormatException } from "../../exceptions/vo-format.exception";
import { TitleVO } from "./title.vo";

describe("Title Value-Object", () => {
  test("should create", () => {
    const titleValue = "Lorem ipsum dolor sit amet";
    const title = TitleVO.create(titleValue);
    expect(title.value).toBe(titleValue);
  });

  test("should remove empty spaces", () => {
    const titleValue = "             Lorem ipsum dolor sit amet";
    const title = TitleVO.create(titleValue);
    expect(title.value).toBe("Lorem ipsum dolor sit amet");
  });

  test("should throw an Error if length is lower than 2", () => {
    const titleValue = "W";

    const errorThrower = () => TitleVO.create(titleValue);
    expect(errorThrower).toThrowError(VOFormatException);
    expect(errorThrower).toThrowError(
      "TitleVO -> Invalid value 'W' (must have 2 or more characters)"
    );
  });
});
