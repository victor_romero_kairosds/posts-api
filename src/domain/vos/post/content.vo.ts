import { VOFormatException } from "../../exceptions/vo-format.exception";
import { OffensiveWord } from "../../models/offensive-word.entity";

export class ContentVO {
  private constructor(private content: string) {}

  static create(content: string, offensiveWords?: OffensiveWord[]): ContentVO {
    const trimmedContent = content.trim();

    if (trimmedContent.length < 2) {
      throw new VOFormatException(
        ContentVO.name,
        content,
        "must have 2 or more characters"
      );
    }

    if (offensiveWords) {
      const splittedWords = content
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")
        .toLowerCase()
        .trim()
        .replace(/[^a-z0-9 ]/g, "")
        .split(" ");

      const offensiveWordsList = offensiveWords.map((word) => word.word.value);

      offensiveWordsList.forEach((ow) => {
        const hasOffensiveWord = splittedWords.includes(ow);
        if (hasOffensiveWord) {
          throw new VOFormatException(
            ContentVO.name,
            ow,
            "cannot have any offensive word"
          );
        }
      });
    }

    return new ContentVO(trimmedContent);
  }

  get value(): string {
    return this.content;
  }
}
