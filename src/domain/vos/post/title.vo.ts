import { VOFormatException } from "../../exceptions/vo-format.exception";

export class TitleVO {
  private constructor(private title: string) {}

  static create(title: string) {
    const trimmedTitle = title.trim();
    if (trimmedTitle.length < 2) {
      throw new VOFormatException(
        TitleVO.name,
        trimmedTitle,
        "must have 2 or more characters"
      );
    }
    return new TitleVO(trimmedTitle);
  }

  get value(): string {
    return this.title;
  }
}
