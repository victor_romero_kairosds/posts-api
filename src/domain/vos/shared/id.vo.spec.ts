import { v4, validate } from "uuid";

import { VOFormatException } from "../../exceptions/vo-format.exception";
import { IdVO } from "./id.vo";

describe("IdVO", () => {
  test("should create a valid id", () => {
    const id = IdVO.create();
    expect(validate(id.value)).toBeTruthy();
  });

  test("should created a id from a valid uuid", () => {
    const id = IdVO.createWithUUID(v4());
    expect(validate(id.value)).toBeTruthy();
  });

  test("should throw an Error if a non valid id is provided", () => {
    const invalidUUID = "1234-1234-1234-1234";
    const errorThrower = () => IdVO.createWithUUID(invalidUUID);
    expect(errorThrower).toThrowError(VOFormatException);
    expect(errorThrower).toThrowError(
      `IdVO -> Invalid value '${invalidUUID}' (have to provide a valid UUID)`
    );
  });
});
