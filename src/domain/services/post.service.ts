import { Inject, Service } from "typedi";
import { EntityNotFoundException } from "../exceptions/not-found.exception";
import { Comment, CommentType } from "../models/comment.entity";
import { Post, PostType } from "../models/post.entity";
import { PostRepository } from "../repositories/post.repository";
import { IdVO } from "../vos/shared/id.vo";

@Service()
export class PostService {
  constructor(@Inject("post-repository") private repository: PostRepository) {}

  async persist(post: PostType): Promise<void> {
    return this.repository.save(new Post(post));
  }

  async findAll(): Promise<Post[]> {
    return this.repository.findAll();
  }

  async findById(id: IdVO): Promise<Post> {
    const post = await this.repository.findById(id);
    if (!post) throw new EntityNotFoundException(PostService.name, "post");
    return post;
  }

  async updateById(post: Post): Promise<void> {
    return this.repository.updateById(new Post(post));
  }

  async deleteById(id: IdVO): Promise<void> {
    return this.repository.deleteById(id);
  }

  async deleteAll(): Promise<void> {
    return this.repository.deleteAll();
  }

  async addComment(comment: CommentType, postId: IdVO): Promise<void> {
    const post = await this.findById(postId);
    return this.repository.addComment(new Comment(comment), post);
  }

  async updateComment(comment: CommentType, postId: IdVO): Promise<void> {
    await this.findById(postId);
    await this.findComment(comment.id);
    return this.repository.updateComment(new Comment(comment));
  }

  async removeComment(commentId: IdVO, postId: IdVO): Promise<void> {
    await this.findById(postId);
    await this.findComment(commentId);
    return this.repository.removeComment(commentId);
  }

  async findComment(commentId: IdVO): Promise<Comment> {
    const comment = await this.repository.findCommentById(commentId);

    if (!comment)
      throw new EntityNotFoundException(PostService.name, "comment");

    return comment;
  }
}
