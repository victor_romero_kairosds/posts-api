import { Inject, Service } from "typedi";
import { EntityNotFoundException } from "../exceptions/not-found.exception";
import {
  OffensiveWord,
  OffensiveWordType,
} from "../models/offensive-word.entity";
import { OffensiveWordRepository } from "../repositories/offensive-word.repository";
import { IdVO } from "../vos/shared/id.vo";

@Service()
export class OffensiveWordService {
  constructor(
    @Inject("offensive-word-repository")
    private repository: OffensiveWordRepository
  ) {}

  async persist(word: OffensiveWordType): Promise<void> {
    return this.repository.save(new OffensiveWord(word));
  }

  async findAll(): Promise<OffensiveWord[]> {
    return this.repository.findAll();
  }

  async findById(id: IdVO): Promise<OffensiveWord> {
    const offensiveWord = await this.repository.findById(id);
    if (!offensiveWord)
      throw new EntityNotFoundException(
        OffensiveWordService.name,
        "offensive-word"
      );
    return offensiveWord;
  }

  async updateById(word: OffensiveWordType): Promise<void> {
    return this.repository.updateById(new OffensiveWord(word));
  }

  async deleteById(id: IdVO): Promise<void> {
    return this.repository.deleteById(id);
  }

  async deleteAll(): Promise<void> {
    return this.repository.deleteAll();
  }
}
