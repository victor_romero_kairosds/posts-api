import { Comment } from "../models/comment.entity";
import { Post } from "../models/post.entity";
import { IdVO } from "../vos/shared/id.vo";

export interface PostRepository {
  save(post: Post): Promise<void>;
  findAll(): Promise<Post[]>;
  findById(id: IdVO): Promise<Post | null>;
  deleteById(id: IdVO): Promise<void>;
  deleteAll(): Promise<void>;
  updateById(post: Post): Promise<void>;
  addComment(comment: Comment, post: Post): Promise<void>;
  updateComment(comment: Comment): Promise<void>;
  removeComment(commentId: IdVO): Promise<void>;
  findCommentById(commentId: IdVO): Promise<Comment | null>;
}
