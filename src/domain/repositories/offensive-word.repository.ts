import { OffensiveWord } from "../models/offensive-word.entity";
import { IdVO } from "../vos/shared/id.vo";

export interface OffensiveWordRepository {
  save(word: OffensiveWord): Promise<void>;

  findAll(): Promise<OffensiveWord[]>;

  findById(id: IdVO): Promise<OffensiveWord | null>;

  deleteById(id: IdVO): Promise<void>;

  deleteAll(): Promise<void>;

  updateById(word: OffensiveWord): Promise<void>;
}
