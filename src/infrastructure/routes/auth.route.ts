import { Router } from "express";
import { body } from "express-validator";
import passport from "passport";
import { User } from "../../domain/models/user.entity";
import { Role } from "../../domain/vos/user/role.vo";
import { deleteAllUserController } from "../controllers/auth/delete-all-user.controller";
import { signInController } from "../controllers/auth/sign-in.controller";
import { signUpController } from "../controllers/auth/sign-up.controller";
import { hasRole } from "../middlewares/roles.middleware";

const router = Router();

router.post(
  "/auth/sign-up",
  body("email").notEmpty().isString(),
  body("password").notEmpty(),
  body("role").notEmpty(),
  signUpController
);

router.post(
  "/auth/login",
  body("email").notEmpty().isString(),
  body("password").notEmpty(),
  signInController
);

router.get(
  "/auth/role/me",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const user = req.user as User;
    res.status(200).send({ role: user.role.value });
  }
);

router.get(
  "/auth/data/me",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const user = req.user as User;
    res.status(200).send({
      id: user.id.value,
      email: user.email.value,
    });
  }
);

router.delete(
  "/auth",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  deleteAllUserController
);

export { router as AuthRouter };
