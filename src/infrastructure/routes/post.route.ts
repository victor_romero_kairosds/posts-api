import { Router } from "express";
import { body } from "express-validator";
import passport from "passport";

import { Role } from "../../domain/vos/user/role.vo";
import { postCache } from "../cache/post-cache.middleware";
import { createCommentController } from "../controllers/post/create-comment.controller";
import { createPostController } from "../controllers/post/create-post.controller";
import { deleteAllPostController } from "../controllers/post/delete-all-post.controller";
import { deleteCommentController } from "../controllers/post/delete-comment.controller";
import { deletePostByIdController } from "../controllers/post/delete-post-by-id.controller";
import { getAllPostController } from "../controllers/post/get-all-post.controller";
import { getCommentByIdController } from "../controllers/post/get-comment-by-id.controller";
import { getPostByIdController } from "../controllers/post/get-post-by-id.controller";
import { updatePostByIdController } from "../controllers/post/update-by-id.controller";
import { updateCommentController } from "../controllers/post/update-comment-by-id.controller";
import { hasAuthorization } from "../middlewares/authorization.middleware";
import { hasRole } from "../middlewares/roles.middleware";
import { Resource } from "../types/resource.type";

const router = Router();

router.get(
  "/post",
  postCache({ handleHttpResponse: true }),
  getAllPostController
);

router.get(
  "/post/:id",
  postCache({ handleHttpResponse: true }),
  getPostByIdController
);

router.post(
  "/post",
  passport.authenticate("jwt", { session: false, failWithError: true }),
  hasRole(Role.AUTHOR, Role.ADMIN),
  body("title").notEmpty().isString(),
  body("content").notEmpty().isString(),
  postCache(),
  createPostController
);
router.delete(
  "/post",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  deleteAllPostController
);
router.delete(
  "/post/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.AUTHOR, Role.ADMIN),
  hasAuthorization(Resource.POST),
  postCache(),
  deletePostByIdController
);
router.put(
  "/post/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.AUTHOR, Role.ADMIN),
  hasAuthorization(Resource.POST),
  body("title").notEmpty().isString(),
  body("content").notEmpty().isString(),
  postCache(),
  updatePostByIdController
);

// Comments
router.get(
  "/post/:postId/comment/:commentId",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.USER, Role.ADMIN, Role.AUTHOR),
  getCommentByIdController
);
router.post(
  "/post/:postId/comment",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.USER, Role.ADMIN, Role.AUTHOR),
  body("content").notEmpty().isString(),
  postCache(),
  createCommentController
);
router.put(
  "/post/:postId/comment/:commentId",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.USER, Role.ADMIN, Role.AUTHOR),
  body("content").notEmpty().isString(),
  postCache(),
  updateCommentController
);
router.delete(
  "/post/:postId/comment/:commentId",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.USER, Role.ADMIN, Role.AUTHOR),
  hasAuthorization(Resource.COMMENT),
  postCache(),
  deleteCommentController
);

export { router as PostRouter };
