import { Router } from "express";
import { body } from "express-validator";
import passport from "passport";
import { Role } from "../../domain/vos/user/role.vo";
import { createOffensiveWordController } from "../controllers/offensive-word/create-offensive-word.controller";
import { deleteAllOffensiveWordController } from "../controllers/offensive-word/delete-all-offensive-word.controller";
import { deleteOffensiveWordByIdController } from "../controllers/offensive-word/delete-offensive-word-by-id.controller";
import { getAllOffensiveWordController } from "../controllers/offensive-word/get-all-offensive-word.controller";
import { getOffensiveWordByIdController } from "../controllers/offensive-word/get-offensive-word-by-id.controller";
import { updateOffensiveWordByIdController } from "../controllers/offensive-word/update-offensive-word-by-id.controller";
import { hasRole } from "../middlewares/roles.middleware";

const router = Router();

router.get(
  "/offensive-word/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  getOffensiveWordByIdController
);

router.get(
  "/offensive-word",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  getAllOffensiveWordController
);

router.delete(
  "/offensive-word/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  deleteOffensiveWordByIdController
);

router.delete(
  "/offensive-word",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  deleteAllOffensiveWordController
);

router.post(
  "/offensive-word",
  body("word").isString().trim().notEmpty(),
  body("level").isNumeric().notEmpty(),
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  createOffensiveWordController
);

router.put(
  "/offensive-word/:id",
  body("word").isString().trim().notEmpty(),
  body("level").isNumeric().notEmpty(),
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  updateOffensiveWordByIdController
);

export { router as OffensiveWordRouter };
