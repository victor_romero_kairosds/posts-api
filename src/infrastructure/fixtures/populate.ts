import Container from "typedi";
import { User, UserType } from "../../domain/models/user.entity";
import { UserService } from "../../domain/services/user.service";
import { IdVO } from "../../domain/vos/shared/id.vo";
import { EmailVO } from "../../domain/vos/user/email.vo";
import { PasswordVO } from "../../domain/vos/user/password.vo";
import { Role, RoleVO } from "../../domain/vos/user/role.vo";

export async function populateDatabase(): Promise<void> {
  const userService = Container.get(UserService);

  try {
    await userService.findByEmail(EmailVO.create("admin@test.com"));
  } catch (error) {
    const adminData: UserType = {
      id: IdVO.create(),
      email: EmailVO.create("admin@test.com"),
      password: PasswordVO.create("admin"),
      role: RoleVO.create(Role.ADMIN),
    };
    await userService.persist(new User(adminData));
  }
}
