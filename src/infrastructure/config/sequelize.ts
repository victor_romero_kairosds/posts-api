import { Sequelize } from "sequelize";
import { populateDatabase } from "../fixtures/populate";

const HOST = process.env.PG_HOST ?? "localhost";
const PORT = process.env.PG_PORT ?? "5432";
const DB_NAME = process.env.PG_DB_NAME ?? "pgdb";
const USER = process.env.PG_USER ?? "pguser";
const PASSWORD = process.env.PG_PASSWORD ?? "pguser";

const sequelize = new Sequelize(
  `postgres://${USER}:${PASSWORD}@${HOST}:${PORT}/${DB_NAME}`,
  { logging: false }
);

sequelize
  .authenticate()
  .then(() => console.log("✅ Connection with PostgresSQL established"))
  .catch((err) => console.log(err));

sequelize
  .sync({ alter: { drop: false } })
  .then(() => {
    console.log("✅ Database sync without dropping tables");
  })
  .catch(() => {
    sequelize
      .sync({ force: true })
      .then(() => console.log("❗Database sync by dropping tables"));
  })
  .finally(() => populateDatabase());

export default sequelize;
