import mongoose from "mongoose";

export async function connectToDB() {
  const {
    MONGO_HOST,
    MONGO_PORT,
    MONGO_DB,
    MONGO_AUTH_SOURCE,
    MONGO_ADMIN,
    MONGO_PASSWORD,
  } = process.env;

  const options: mongoose.ConnectOptions = {
    authSource: MONGO_AUTH_SOURCE,
    auth: {
      username: MONGO_ADMIN,
      password: MONGO_PASSWORD,
    },
    connectTimeoutMS: 4000,
  };

  await mongoose
    .connect(`mongodb://${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}`, options)
    .then(() => {
      console.log("✅ Connection with MongoDB established");
    });

  mongoose.connection.on("error", (error) => {
    console.log("❌ Error with MongoDB connection", error);
    process.exit(0);
  });
}

export async function disconnectFromDB() {
  mongoose.disconnect();
}
