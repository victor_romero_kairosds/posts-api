import { InfrastructureFormatException } from "./infrastructure-format.exception";

export class NotAllowedException extends InfrastructureFormatException {
  constructor(constructorName: string) {
    super(`${constructorName} -> Action not allowed`);
  }
}
