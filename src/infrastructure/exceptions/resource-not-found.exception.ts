import { InfrastructureFormatException } from "./infrastructure-format.exception";

export class ResourceNotFoundException extends InfrastructureFormatException {
  constructor(constructorName: string, resource: string) {
    super(`${constructorName} -> Resource ${resource} not found`);
  }
}
