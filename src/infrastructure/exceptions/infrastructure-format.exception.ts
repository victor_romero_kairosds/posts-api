import { CustomError } from "ts-custom-error";

export class InfrastructureFormatException extends CustomError {}
