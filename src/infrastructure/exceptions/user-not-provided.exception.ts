import { InfrastructureFormatException } from "./infrastructure-format.exception";

export class UserNotProvided extends InfrastructureFormatException {
  constructor(constructorName: string) {
    super(
      `${constructorName} -> Need to provide a user in Request to use this middleware`
    );
  }
}
