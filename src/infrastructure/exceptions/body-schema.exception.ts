import { ValidationError } from "express-validator";
import { InfrastructureFormatException } from "./infrastructure-format.exception";

export class BodySchemaException extends InfrastructureFormatException {
  constructor(errors: ValidationError[]) {
    const wrongValues = errors
      .map((err) =>
        typeof err.value === "string"
          ? `'${err.value}'`
          : `'${JSON.stringify(err.value)}'`
      )
      .join(", ");
    const wrongParams = errors.map((err) => `'${err.param}'`).join(", ");
    const errorMessage = `Invalid values ${wrongValues} at ${wrongParams}`;
    super(errorMessage);
  }
}
