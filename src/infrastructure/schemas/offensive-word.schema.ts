import mongoose from "mongoose";

const Schema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
  },
  word: {
    type: String,
    required: true,
  },
  level: {
    type: Number,
    required: true,
  },
});

export const OffensiveWordModel = mongoose.model("OffensiveWord", Schema);
