import { CommentModel } from "./comment.schema";
import { PostModel } from "./post.schema";
import { UserModel } from "./user.schema";

export function createAssociations() {
  UserModel.hasMany(CommentModel);
  CommentModel.belongsTo(UserModel);

  PostModel.hasMany(CommentModel);
  CommentModel.belongsTo(PostModel);

  UserModel.hasMany(PostModel);
  PostModel.belongsTo(UserModel);
}
