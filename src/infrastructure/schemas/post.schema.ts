import { DataTypes } from "sequelize";
import sequelize from "../config/sequelize";

export const PostModel = sequelize.define("post", {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true,
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  content: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});
