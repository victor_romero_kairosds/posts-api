import { DataTypes } from "sequelize";
import sequelize from "../config/sequelize";
import { CommentModel } from "./comment.schema";
import { PostModel } from "./post.schema";

export const UserModel = sequelize.define("user", {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  role: {
    type: DataTypes.ENUM("ADMIN", "USER", "AUTHOR"),
    allowNull: false,
  },
});

UserModel.hasMany(CommentModel);
CommentModel.belongsTo(UserModel);

PostModel.hasMany(CommentModel);
CommentModel.belongsTo(PostModel);

UserModel.hasMany(PostModel);
PostModel.belongsTo(UserModel);
