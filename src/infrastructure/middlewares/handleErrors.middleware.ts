import { ErrorRequestHandler, NextFunction, Request, Response } from "express";
import { Error as SequelizeError } from "sequelize";
import { EntityNotFoundException } from "../../domain/exceptions/not-found.exception";
import { PasswordMissmatchException } from "../../domain/exceptions/password-missmatch.exception";
import { VOFormatException } from "../../domain/exceptions/vo-format.exception";
import { logger } from "../config/logger";
import { NotAllowedException } from "../exceptions/action-not-allowed.exception";
import { BodySchemaException } from "../exceptions/body-schema.exception";
import { ResourceNotFoundException } from "../exceptions/resource-not-found.exception";
import { ErrorJSONResponse } from "../types/json-response.type";

export async function handleErrorsMiddleware(
  error: ErrorRequestHandler,
  _req: Request,
  res: Response,
  _next: NextFunction
) {
  console.log(error);
  logger.error(error);
  if (error instanceof BodySchemaException) {
    const response: ErrorJSONResponse = {
      success: false,
      error_message: error.message,
    };
    return res.status(400).send(response);
  }

  if (error instanceof VOFormatException) {
    const { value, rule } = error;
    const response: ErrorJSONResponse = {
      success: false,
      error_message: `Invalid value ${value} (${rule})`,
    };
    return res.status(400).send(response);
  }

  if (
    error instanceof ResourceNotFoundException ||
    error instanceof EntityNotFoundException
  ) {
    const response: ErrorJSONResponse = {
      success: false,
      error_message: "The resource you requested could not be found",
    };
    return res.status(404).send(response);
  }
  if (error instanceof NotAllowedException) {
    const response: ErrorJSONResponse = {
      success: false,
      error_message: "Action not allowed",
    };
    return res.status(403).send(response);
  }

  if (error instanceof PasswordMissmatchException) {
    const response: ErrorJSONResponse = {
      success: false,
      error_message: "Wrong credentials",
    };
    return res.status(401).send(response);
  }

  if (error instanceof SequelizeError) {
    const response: ErrorJSONResponse = {
      success: false,
      error_message: "Bad request",
    };
    return res.status(400).send(response);
  }
  const defaultResponse: ErrorJSONResponse = {
    success: false,
    error_message: "Internal server error",
  };
  return res.status(500).send(defaultResponse);
}
