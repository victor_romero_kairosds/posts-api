import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";
import Container from "typedi";
import { UserService } from "../../domain/services/user.service";
import { EmailVO } from "../../domain/vos/user/email.vo";
import { logger } from "../config/logger";

const options: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: "SECRET",
};

export default new Strategy(options, async (payload, done) => {
  try {
    const { email } = payload;
    const userService = Container.get(UserService);
    const user = await userService.findByEmail(EmailVO.create(email));
    if (user) {
      return done(null, user);
    }
    return done(null, false, { message: "token not valid" });
  } catch (err) {
    logger.error(err);
  }
});
