import { NextFunction, Request, Response } from "express";
import { User } from "../../domain/models/user.entity";
import { Role } from "../../domain/vos/user/role.vo";
import { NotAllowedException } from "../exceptions/action-not-allowed.exception";

export function hasRole(...roles: Role[]) {
  return async function (req: Request, _res: Response, next: NextFunction) {
    const user = req.user as User;
    const roleUser = user.role.value;

    const isAllowed = roles.some((role) => roleUser === role);
    if (isAllowed) return next();

    throw new NotAllowedException(hasRole.name);
  };
}
