import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { User } from "../../domain/models/user.entity";
import { PostService } from "../../domain/services/post.service";
import { IdVO } from "../../domain/vos/shared/id.vo";
import { NotAllowedException } from "../exceptions/action-not-allowed.exception";
import { ResourceNotFoundException } from "../exceptions/resource-not-found.exception";
import { UserNotProvided } from "../exceptions/user-not-provided.exception";
import { Resource } from "../types/resource.type";

export function hasAuthorization(resourceToCheck: Resource) {
  return async function (req: Request, _res: Response, next: NextFunction) {
    const user = req.user as User;
    if (!user) throw new UserNotProvided(hasAuthorization.name);

    if (user.role.value === "ADMIN") return next();

    // eslint-disable-next-line no-unused-vars
    type actionsType = { [key in keyof typeof Resource]: any };

    const actions: actionsType = {
      COMMENT: () => checkCommentAuthor(req),
      POST: () => checkPostAuthor(req),
    };

    try {
      await actions[resourceToCheck]();
      next();
    } catch (error) {
      next(error);
    }
  };
}

async function checkPostAuthor(req: Request) {
  const postId = req.params.id;
  const user = req.user as User;
  const postService = Container.get(PostService);

  const post = await postService.findById(IdVO.createWithUUID(postId));

  if (!post) throw new ResourceNotFoundException(hasAuthorization.name, "post");

  if (post.author.id.value !== user.id.value)
    throw new NotAllowedException(hasAuthorization.name);
}

async function checkCommentAuthor(req: Request) {
  const commentId = req.params.commentId;
  const user = req.user as User;
  const postService = Container.get(PostService);

  const comment = await postService.findComment(IdVO.createWithUUID(commentId));

  if (!comment)
    throw new ResourceNotFoundException(hasAuthorization.name, "comment");

  if (comment.author.id.value !== user.id.value)
    throw new NotAllowedException(hasAuthorization.name);
}
