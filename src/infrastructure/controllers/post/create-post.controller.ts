import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import Container from "typedi";
import { CreatePostUseCase } from "../../../application/use-cases/post/create-post.usecase";
import { User } from "../../../domain/models/user.entity";
import { BodySchemaException } from "../../exceptions/body-schema.exception";

export async function createPostController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new BodySchemaException(errors.array()));
  }
  const { title, content } = req.body;
  const author = req.user as User;
  try {
    const useCase = Container.get(CreatePostUseCase);
    const newPost = await useCase.execute({ title, content, author });
    res.locals.data = { value: newPost, key: newPost.id };
    return res.status(201).send(newPost);
  } catch (err) {
    return next(err);
  }
}
