import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { DeleteAllPostUseCase } from "../../../application/use-cases/post/delete-all-post.usecase";

export async function deleteAllPostController(
  _req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(DeleteAllPostUseCase);
    await useCase.execute();
    return res.sendStatus(204);
  } catch (err) {
    return next(err);
  }
}
