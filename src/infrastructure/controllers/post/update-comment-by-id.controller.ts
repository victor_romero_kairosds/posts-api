import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { UpdateCommentByIdUseCase } from "../../../application/use-cases/post/update-comment-by-id.usecase";
import { User } from "../../../domain/models/user.entity";
import { BodySchemaException } from "../../exceptions/body-schema.exception";

export async function updateCommentController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new BodySchemaException(errors.array()));
  }

  const postId: IdInput = req.params.postId;
  const commentId: IdInput = req.params.commentId;

  const { content } = req.body;
  try {
    const useCase = Container.get(UpdateCommentByIdUseCase);
    const user = req.user as User;

    const updatedComment = await useCase.execute(
      { content, author: user },
      commentId,
      postId
    );
    res.locals.data = { value: null, key: postId };

    return res.status(200).send(updatedComment);
  } catch (err) {
    return next(err);
  }
}
