import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { GetCommentByIdUseCase } from "../../../application/use-cases/post/get-comment-by-id.usecase";

export async function getCommentByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(GetCommentByIdUseCase);
    const id: IdInput = req.params.commentId;
    const comment = await useCase.execute(id);
    return res.status(200).send(comment);
  } catch (err) {
    return next(err);
  }
}
