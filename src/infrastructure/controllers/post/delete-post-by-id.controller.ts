import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { DeletePostByIdUseCase } from "../../../application/use-cases/post/delete-post-by-id.usecase";

export async function deletePostByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(DeletePostByIdUseCase);
    const id: IdInput = req.params.id;
    await useCase.execute(id);
    res.locals.data = { value: null, key: id };
    return res.status(204).send();
  } catch (err) {
    return next(err);
  }
}
