import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { GetAllPostUseCase } from "../../../application/use-cases/post/get-all-post.usecase";

export async function getAllPostController(
  _req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(GetAllPostUseCase);
    const posts = await useCase.execute();
    res.locals.data = { value: posts, key: "all" };
    return res.status(200).send(posts);
  } catch (err) {
    return next(err);
  }
}
