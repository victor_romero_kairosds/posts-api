import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { UpdatePostByIdUseCase } from "../../../application/use-cases/post/update-post-by-id.usecase";
import { User } from "../../../domain/models/user.entity";
import { BodySchemaException } from "../../exceptions/body-schema.exception";

export async function updatePostByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return next(new BodySchemaException(errors.array()));
    }
    const useCase = Container.get(UpdatePostByIdUseCase);
    const id: IdInput = req.params.id;
    const author = req.user as User;

    const { title, content } = req.body;
    const post = await useCase.execute(id, { title, content, author });
    res.locals.data = { value: post, key: post.id };
    return res.status(200).send(post);
  } catch (err) {
    return next(err);
  }
}
