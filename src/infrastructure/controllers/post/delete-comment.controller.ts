import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { RemoveCommentUseCase } from "../../../application/use-cases/post/remove-comment.usecase";
import { BodySchemaException } from "../../exceptions/body-schema.exception";

export async function deleteCommentController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new BodySchemaException(errors.array()));
  }
  const commentId: IdInput = req.params.commentId;
  const postId: IdInput = req.params.postId;

  try {
    const useCase = Container.get(RemoveCommentUseCase);
    await useCase.execute(commentId, postId);
    res.locals.data = { value: null, key: postId };
    return res.sendStatus(204);
  } catch (err) {
    return next(err);
  }
}
