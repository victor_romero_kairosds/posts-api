import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { GetPostByIdUseCase } from "../../../application/use-cases/post/get-post-by-id.usecase";

export async function getPostByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(GetPostByIdUseCase);
    const id: IdInput = req.params.id;
    const post = await useCase.execute(id);
    res.locals.data = { value: post, key: post.id };
    return res.status(200).send(post);
  } catch (err) {
    return next(err);
  }
}
