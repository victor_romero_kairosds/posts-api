import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { AddCommentUseCase } from "../../../application/use-cases/post/add-comment.usecase";
import { User } from "../../../domain/models/user.entity";
import { BodySchemaException } from "../../exceptions/body-schema.exception";

export async function createCommentController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new BodySchemaException(errors.array()));
  }

  const postId: IdInput = req.params.postId;

  const { content } = req.body;
  try {
    const useCase = Container.get(AddCommentUseCase);
    const user = req.user as User;

    const newComment = await useCase.execute({ content, author: user }, postId);
    res.locals.data = { value: null, key: postId };

    return res.status(201).send(newComment);
  } catch (err) {
    return next(err);
  }
}
