import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import Container from "typedi";
import { SignUpInput } from "../../../application/types/auth.type";
import { SignUpUseCase } from "../../../application/use-cases/auth/sign-up.usecase";
import { BodySchemaException } from "../../exceptions/body-schema.exception";

export async function signUpController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new BodySchemaException(errors.array()));
  }
  const { email, password, role } = req.body;

  const signUpRequest: SignUpInput = {
    email,
    password,
    role,
  };

  try {
    const useCase = Container.get(SignUpUseCase);
    const token = await useCase.execute(signUpRequest);
    return res.status(201).send(token);
  } catch (err) {
    return next(err);
  }
}
