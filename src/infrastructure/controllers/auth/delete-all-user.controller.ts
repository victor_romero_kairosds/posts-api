import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { DeleteAllUsersUseCase } from "../../../application/use-cases/auth/delete-all-user.usecase";

export async function deleteAllUserController(
  _req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(DeleteAllUsersUseCase);
    await useCase.execute();
    return res.sendStatus(204);
  } catch (err) {
    return next(err);
  }
}
