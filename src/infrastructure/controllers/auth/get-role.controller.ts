import { Request, Response } from "express";
import { User } from "../../../domain/models/user.entity";

export async function getRoleController(req: Request, res: Response) {
  const user = req.user as User;
  res.status(200).send({ role: user.role.value });
}
