import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { DeleteOffensiveWordByIdUseCase } from "../../../application/use-cases/offensive-word/delete-offensive-word-by-id.usecase";

export async function deleteOffensiveWordByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(DeleteOffensiveWordByIdUseCase);
    const id: IdInput = req.params.id;
    await useCase.execute(id);
    return res.status(204).send({});
  } catch (err) {
    return next(err);
  }
}
