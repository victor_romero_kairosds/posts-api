import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import Container from "typedi";
import { OffensiveWordInput } from "../../../application/types/offensive-word.type";
import { CreateOffensiveWordUseCase } from "../../../application/use-cases/offensive-word/create-offensive-word.usecase";
import { BodySchemaException } from "../../exceptions/body-schema.exception";

export async function createOffensiveWordController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new BodySchemaException(errors.array()));
  }
  const { word, level } = req.body;

  const offensiveWordRequest: OffensiveWordInput = {
    word,
    level,
  };

  try {
    const useCase = Container.get(CreateOffensiveWordUseCase);
    const newOffensiveWord = await useCase.execute(offensiveWordRequest);
    return res.status(201).send(newOffensiveWord);
  } catch (err) {
    return next(err);
  }
}
