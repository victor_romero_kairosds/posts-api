import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { GetOffensiveWordByIdUseCase } from "../../../application/use-cases/offensive-word/get-offensive-word-by-id.usecase";

export async function getOffensiveWordByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(GetOffensiveWordByIdUseCase);
    const id: IdInput = req.params.id;
    const offensiveWord = await useCase.execute(id);
    return res.status(200).send(offensiveWord);
  } catch (err) {
    return next(err);
  }
}
