import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { DeleteAllOffensiveWordUseCase } from "../../../application/use-cases/offensive-word/delete-all-offensive-word.usecase";

export async function deleteAllOffensiveWordController(
  _req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(DeleteAllOffensiveWordUseCase);
    await useCase.execute();
    return res.sendStatus(204);
  } catch (err) {
    return next(err);
  }
}
