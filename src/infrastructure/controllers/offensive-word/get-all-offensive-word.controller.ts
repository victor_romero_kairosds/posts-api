import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { GetAllOffensiveWordUseCase } from "../../../application/use-cases/offensive-word/get-all-offensive-word.usecase";

export async function getAllOffensiveWordController(
  _req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const useCase = Container.get(GetAllOffensiveWordUseCase);
    const offensiveWords = await useCase.execute();
    return res
      .status(200)
      .send({ results: offensiveWords, totalResults: offensiveWords.length });
  } catch (err) {
    return next(err);
  }
}
