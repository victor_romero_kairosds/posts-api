import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import Container from "typedi";
import { IdInput } from "../../../application/types/shared.type";
import { UpdateOffensiveWordByIdUseCase } from "../../../application/use-cases/offensive-word/update-offensive-word-by-id.usecase";
import { BodySchemaException } from "../../exceptions/body-schema.exception";

export async function updateOffensiveWordByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new BodySchemaException(errors.array()));
  }

  const { word, level } = req.body;
  const id: IdInput = req.params.id;

  try {
    const useCase = Container.get(UpdateOffensiveWordByIdUseCase);
    const updatedWord = await useCase.execute(id, { word, level });
    return res.status(200).send(updatedWord);
  } catch (err) {
    next(err);
  }
}
