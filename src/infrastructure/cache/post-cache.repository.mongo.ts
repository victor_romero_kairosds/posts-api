import { Service } from "typedi";
import { PostCacheModel } from "./post-cache.schema";

export type CacheInput = {
  key: string;
  data: string;
};

@Service()
export class PostCacheRepository {
  async save(cache: CacheInput): Promise<void> {
    const cacheModel = new PostCacheModel({
      ...cache,
      createdAt: new Date().toISOString(),
    });
    await cacheModel.save();
  }

  async clear(key: string): Promise<void> {
    await PostCacheModel.deleteMany({ key });
  }

  async getByKey(
    key: string
  ): Promise<{ key: string; data: string; createdAt: Date } | null> {
    const cache = await PostCacheModel.findOne({ key });
    if (!cache) return null;
    return cache;
  }
}
