import mongoose from "mongoose";

const Schema = new mongoose.Schema({
  key: {
    type: String,
    required: true,
  },
  data: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    required: true,
  },
});

export const PostCacheModel = mongoose.model("Cache", Schema);
