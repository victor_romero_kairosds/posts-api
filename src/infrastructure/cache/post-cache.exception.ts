import { InfrastructureFormatException } from "../exceptions/infrastructure-format.exception";

export class PostCacheException extends InfrastructureFormatException {
  constructor(constructorName: string, error: any) {
    super(`${constructorName} -> Cache error ${error.name}`);
  }
}
