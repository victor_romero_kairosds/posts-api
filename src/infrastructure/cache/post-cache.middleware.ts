import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { PostCacheRepository } from "./post-cache.repository.mongo";

export type CACHE_OPTIONS = {
  handleHttpResponse?: boolean;
};

export function postCache(
  { handleHttpResponse }: CACHE_OPTIONS = { handleHttpResponse: false }
) {
  return async function (req: Request, res: Response, next: NextFunction) {
    const cacheRepository = Container.get(PostCacheRepository);

    res.on("finish", async () => {
      if (res.locals.data) {
        const { value, key } = res.locals.data;
        await cacheRepository.clear(key);
        await cacheRepository.clear("all");
        if (req.method !== "DELETE" && value) {
          await cacheRepository.save({
            key,
            data: JSON.stringify(value),
          });
        }
      }
    });

    if (handleHttpResponse) {
      const key = req.params.id ?? "all";
      const cache = await cacheRepository.getByKey(key);
      if (!cache) return next();
      const { createdAt, data } = cache;
      const isCacheExpired =
        (Date.now() - new Date(createdAt).getTime()) / 1000 / 60 > 10;
      if (isCacheExpired) return next();
      const jsonData = JSON.parse(data);
      return res.status(200).send(jsonData);
    }

    next();
  };
}
