import { OffensiveWord } from "../../domain/models/offensive-word.entity";
import { OffensiveWordRepository } from "../../domain/repositories/offensive-word.repository";
import { LevelVO } from "../../domain/vos/offensive-word/level.vo";
import { WordVO } from "../../domain/vos/offensive-word/word.vo";
import { IdVO } from "../../domain/vos/shared/id.vo";
import { OffensiveWordModel } from "../schemas/offensive-word.schema";

export class OffensiveWordRepositoryMongo implements OffensiveWordRepository {
  async save(offensiveWord: OffensiveWord): Promise<void> {
    const newWord = {
      id: offensiveWord.id.value,
      word: offensiveWord.word.value,
      level: offensiveWord.level.value,
    };

    const model = new OffensiveWordModel(newWord);
    await model.save();
  }

  async findAll(): Promise<OffensiveWord[]> {
    const listOfWords = await OffensiveWordModel.find({}).exec();

    return listOfWords.map((wordInDB) => {
      return new OffensiveWord({
        id: IdVO.createWithUUID(wordInDB.id),
        level: LevelVO.create(wordInDB.level),
        word: WordVO.create(wordInDB.word),
      });
    });
  }

  async findById(id: IdVO): Promise<OffensiveWord | null> {
    const wordInDB = await OffensiveWordModel.findOne({ id: id.value }).exec();

    if (!wordInDB) return null;

    return new OffensiveWord({
      id: IdVO.createWithUUID(wordInDB.id),
      word: WordVO.create(wordInDB.word),
      level: LevelVO.create(wordInDB.level),
    });
  }

  async deleteById(id: IdVO): Promise<void> {
    await OffensiveWordModel.findOneAndDelete({
      id: id.value,
    });
  }

  async deleteAll(): Promise<void> {
    await OffensiveWordModel.deleteMany({});
  }

  async updateById(word: OffensiveWord): Promise<void> {
    const wordToUpdate = {
      id: word.id.value,
      word: word.word.value,
      level: word.level.value,
    };

    await OffensiveWordModel.findOneAndUpdate(
      { id: word.id.value },
      wordToUpdate,
      { new: true }
    );
  }
}
