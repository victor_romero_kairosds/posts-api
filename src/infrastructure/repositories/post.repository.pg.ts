import { Comment } from "../../domain/models/comment.entity";
import { Post } from "../../domain/models/post.entity";
import { User } from "../../domain/models/user.entity";
import { PostRepository } from "../../domain/repositories/post.repository";
import { ContentVO } from "../../domain/vos/post/content.vo";
import { TitleVO } from "../../domain/vos/post/title.vo";
import { IdVO } from "../../domain/vos/shared/id.vo";
import { EmailVO } from "../../domain/vos/user/email.vo";
import { PasswordVO } from "../../domain/vos/user/password.vo";
import { RoleVO } from "../../domain/vos/user/role.vo";
import { ResourceNotFoundException } from "../exceptions/resource-not-found.exception";
import { CommentModel } from "../schemas/comment.schema";
import { PostModel } from "../schemas/post.schema";
import { UserModel } from "../schemas/user.schema";

export class PostRepositoryPg implements PostRepository {
  async save(post: Post): Promise<void> {
    const newPost = {
      id: post.id.value,
      title: post.title.value,
      content: post.content.value,
      userId: post.author.id.value,
    };
    await PostModel.create(newPost);
  }

  async findAll(): Promise<Post[]> {
    const listOfPosts = await PostModel.findAll({
      include: [UserModel, { model: CommentModel, include: [UserModel] }],
    });

    return listOfPosts.map((postInDB: any) => this.postDtoToEntity(postInDB));
  }

  async findById(id: IdVO): Promise<Post | null> {
    const postInDB: any = await PostModel.findOne({
      where: { id: id.value },
      include: [UserModel, { model: CommentModel, include: [UserModel] }],
    });

    if (!postInDB) return null;

    return this.postDtoToEntity(postInDB);
  }

  async updateById(post: Post): Promise<void> {
    const postToUpdate = {
      id: post.id.value,
      title: post.title.value,
      content: post.content.value,
    };
    await PostModel.update(postToUpdate, { where: { id: postToUpdate.id } });
  }

  async deleteAll(): Promise<void> {
    await PostModel.destroy({ where: {} });
    await CommentModel.destroy({ where: {} });
  }

  async deleteById(id: IdVO): Promise<void> {
    await PostModel.destroy({ where: { id: id.value } });
    await CommentModel.destroy({ where: { postId: id.value } });
  }

  async addComment(comment: Comment, post: Post): Promise<void> {
    await CommentModel.create({
      id: comment.id.value,
      content: comment.content.value,
      userId: comment.author.id.value,
      postId: post.id.value,
    });
  }

  async removeComment(commentId: IdVO): Promise<void> {
    const commentInDB: any = await CommentModel.findOne({
      where: { id: commentId.value },
    });

    if (!commentInDB)
      throw new ResourceNotFoundException(PostRepositoryPg.name, "comment");

    commentInDB.destroy();
  }

  async findCommentById(commentId: IdVO): Promise<Comment | null> {
    const commentInDB: any = await CommentModel.findOne({
      where: { id: commentId.value },
      include: [UserModel],
    });

    if (!commentInDB) return null;

    return new Comment({
      id: IdVO.createWithUUID(commentInDB.id),
      content: ContentVO.create(commentInDB.content),
      author: new User({
        id: IdVO.createWithUUID(commentInDB.user.id),
        email: EmailVO.create(commentInDB.user.email),
        password: PasswordVO.createFromHash(commentInDB.user.password),
        role: RoleVO.create(commentInDB.user.role),
      }),
    });
  }

  async updateComment(comment: Comment): Promise<void> {
    const commentToUpdate = {
      id: comment.id.value,
      content: comment.content.value,
      userId: comment.author.id.value,
    };
    await CommentModel.update(commentToUpdate, {
      where: { id: commentToUpdate.id },
    });
  }

  postDtoToEntity(postInDB: any): Post {
    return new Post({
      id: IdVO.createWithUUID(postInDB.id),
      title: TitleVO.create(postInDB.title),
      content: ContentVO.create(postInDB.content),
      author: new User({
        id: IdVO.createWithUUID(postInDB.user.id),
        email: EmailVO.create(postInDB.user.email),
        password: PasswordVO.createFromHash(postInDB.user.password),
        role: RoleVO.create(postInDB.user.role),
      }),
      comments: postInDB.comments.map((commentInDB: any) =>
        this.commentDtoToEntity(commentInDB)
      ),
    });
  }

  commentDtoToEntity(commentInDB: any): Comment {
    return new Comment({
      id: IdVO.createWithUUID(commentInDB.id),
      content: ContentVO.create(commentInDB.content),
      author: new User({
        id: IdVO.createWithUUID(commentInDB.user.id),
        email: EmailVO.create(commentInDB.user.email),
        password: PasswordVO.createFromHash(commentInDB.user.password),
        role: RoleVO.create(commentInDB.user.role),
      }),
    });
  }
}
