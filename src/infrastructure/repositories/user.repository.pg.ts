import { Op } from "sequelize";
import { User, UserType } from "../../domain/models/user.entity";
import { UserRepository } from "../../domain/repositories/user.repository";
import { IdVO } from "../../domain/vos/shared/id.vo";
import { EmailVO } from "../../domain/vos/user/email.vo";
import { PasswordVO } from "../../domain/vos/user/password.vo";
import { RoleVO } from "../../domain/vos/user/role.vo";
import { UserModel } from "../schemas/user.schema";

export class UserRepositoryPg implements UserRepository {
  async save(user: User): Promise<User> {
    const newUser = {
      id: user.id.value,
      email: user.email.value,
      password: user.password.value,
      role: user.role.value,
    };

    const userModel = UserModel.build(newUser);
    await userModel.save();
    return user;
  }

  async findByEmail(email: EmailVO): Promise<User | null> {
    const user: any = await UserModel.findOne({
      where: { email: email.value },
    });

    if (!user) return null;

    const userData: UserType = {
      id: IdVO.createWithUUID(user?.id),
      email: EmailVO.create(user?.email),
      password: PasswordVO.createFromHash(user?.password),
      role: RoleVO.create(user?.role),
    };
    return new User(userData);
  }

  async deleteAll(): Promise<void> {
    await UserModel.destroy({
      where: { email: { [Op.not]: "admin@test.com" } },
    });
  }
}
