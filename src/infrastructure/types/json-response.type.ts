export type ErrorJSONResponse = {
  success: boolean;
  error_message: string;
};

export type ListJSONResponse = {
  results: any[];
  totalResults: number;
};
