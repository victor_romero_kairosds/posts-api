import { app } from "./app";
import { connectToDB } from "./infrastructure/config/mongo";
import "./infrastructure/config/sequelize";

const PORT = process.env.APP_PORT;

connectToDB();

app.listen(PORT, () => {
  console.log(`✅ App listening on http://localhost:${PORT}/`);
});
