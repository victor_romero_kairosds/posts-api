/* eslint-disable import/first */
import "reflect-metadata";

import { config } from "dotenv";
config();

import cors from "cors";
import express, { Request, Response } from "express";
import expressPinoLogger from "express-pino-logger";
import passport from "passport";
import swaggerUI from "swagger-ui-express";
import Container from "typedi";
import YAML from "yamljs";

import { logger } from "./infrastructure/config/logger";
import { handleErrorsMiddleware } from "./infrastructure/middlewares/handleErrors.middleware";
import passportMiddleware from "./infrastructure/middlewares/passport.middleware";
import { OffensiveWordRepositoryMongo } from "./infrastructure/repositories/offensive-word.repository.mongo";
import { PostRepositoryPg } from "./infrastructure/repositories/post.repository.pg";
import { UserRepositoryPg } from "./infrastructure/repositories/user.repository.pg";
import { AuthRouter } from "./infrastructure/routes/auth.route";
import { OffensiveWordRouter } from "./infrastructure/routes/offensive-word.route";
import { PostRouter } from "./infrastructure/routes/post.route";

Container.set("offensive-word-repository", new OffensiveWordRepositoryMongo());
Container.set("user-repository", new UserRepositoryPg());
Container.set("post-repository", new PostRepositoryPg());

const app = express();
const swaggerDocument = YAML.load("./swagger.yaml");

app.use(express.json());
app.use(expressPinoLogger(logger));

app.use(cors());
app.use(passport.initialize());
passport.use(passportMiddleware);

app.use("/api/v1", AuthRouter);
app.use("/api/v1", OffensiveWordRouter);
app.use("/api/v1", PostRouter);
app.use("/api/v1/status", (_req: Request, res: Response) => {
  return res.status(200).send();
});

app.use("/api/v1/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use(handleErrorsMiddleware);

export { app };
