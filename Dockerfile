FROM node:16-alpine3.16 AS builder
WORKDIR /builder

COPY . .

RUN npm install
RUN npm run build
RUN npm prune --production

FROM node:16-alpine AS production
WORKDIR /app

COPY --from=builder /builder/package*.json ./
COPY --from=builder ./builder/dist ./dist
COPY --from=builder ./builder/swagger.yaml ./swagger.yaml
COPY --from=builder ./builder/node_modules ./node_modules

EXPOSE 3000
CMD ["npm", "start"]
