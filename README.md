# Posts API

## Config environment file

In order to start the application you should create a `.env` file in the root folder, following the `.env.example` example.

## Run in development mode

Open a new terminal and run:

```bash
npm run docker:dev:up
npm run build:watch
```

## Run in production mode

Open a new terminal and run:

```bash
npm run docker:prod:up
```

## Shutdown Docker containers

```bash
npm run docker:prod:down #prod mode
npm run docker:dev:down #dev mode
```
